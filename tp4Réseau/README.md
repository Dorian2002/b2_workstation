# TP4 : Vers un réseau d'entreprise

On va utiliser GNS3 dans ce TP pour se rapprocher d'un cas réel. On va focus sur l'aspect routing/switching, avec du matériel Cisco. On va aussi mettre en place des VLANs.

# Sommaire

- [TP4 : Vers un réseau d'entreprise](#tp4--vers-un-réseau-dentreprise)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist VM Linux](#checklist-vm-linux)
- [I. Dumb switch](#i-dumb-switch)
  - [1. Topologie 1](#1-topologie-1)
  - [2. Adressage topologie 1](#2-adressage-topologie-1)
  - [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
  - [1. Topologie 2](#1-topologie-2)
  - [2. Adressage topologie 2](#2-adressage-topologie-2)
    - [3. Setup topologie 2](#3-setup-topologie-2)
- [III. Routing](#iii-routing)
  - [1. Topologie 3](#1-topologie-3)
  - [2. Adressage topologie 3](#2-adressage-topologie-3)
  - [3. Setup topologie 3](#3-setup-topologie-3)
- [IV. NAT](#iv-nat)
  - [1. Topologie 4](#1-topologie-4)
  - [2. Adressage topologie 4](#2-adressage-topologie-4)
  - [3. Setup topologie 4](#3-setup-topologie-4)

# 0. Prérequis

➜ Les clients seront soit :

- VMs Rocky Linux
- VPCS
  - c'est un truc de GNS pour simuler un client du réseau
  - quand on veut juste un truc capable de faire des pings et rien de plus, c'est parfait
  - ça consomme R en ressources

> Faites bien attention aux logos des machines sur les schémas, et vous verrez clairement quand il faut un VPCS ou une VM.

➜ Les switches Cisco des vIOL2

➜ Les routeurs Cisco des c3640

➜ **Vous ne créerez aucune machine virtuelle au début. Vous les créerez au fur et à mesure que le TP vous le demande.** A chaque fois qu'une nouvelle machine devra être créée, vous trouverez l'emoji 🖥️ avec son nom.

## Checklist VM Linux

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] on force une host-only, juste pour pouvoir SSH
- [x] SSH fonctionnel
- [x] résolution de nom
  - vers internet, quand vous aurez le routeur en place

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Dumb switch

## 1. Topologie 1



## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS
- `ping` un VPCS depuis l'autre

```
PC1> show ip all

NAME   IP/MASK              GATEWAY           MAC                DNS
PC1    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00

PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.733 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.857 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=5.811 ms
```
```
NAME   IP/MASK              GATEWAY           MAC                DNS
PC2    10.1.1.2/24          255.255.255.0     00:50:79:66:68:01
```

# II. VLAN

**Le but dans cette partie va être de tester un peu les *VLANs*.**

On va rajouter **un troisième client** qui, bien que dans le même réseau, sera **isolé des autres grâce aux *VLANs***.

**Les *VLANs* sont une configuration à effectuer sur les *switches*.** C'est les *switches* qui effectuent le blocage.

Le principe est simple :

- déclaration du VLAN sur tous les switches
  - un VLAN a forcément un ID (un entier)
  - bonne pratique, on lui met un nom
- sur chaque switch, on définit le VLAN associé à chaque port
  - genre "sur le port 35, c'est un client du VLAN 20 qui est branché"


## 1. Topologie 2


## 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS
```
PC1> show ip all

NAME   IP/MASK              GATEWAY           MAC                DNS
PC1    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00
```
```
PC2> show ip all

NAME   IP/MASK              GATEWAY           MAC                DNS
PC2    10.1.1.2/24          255.255.255.0     00:50:79:66:68:01
```
```
PC3> show ip all

NAME   IP/MASK              GATEWAY           MAC                DNS
PC3    10.1.1.3/24          255.255.255.0     00:50:79:66:68:02
```
- vérifiez avec des `ping` que tout le monde se ping
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.400 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.835 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=4.947 ms
```
```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.130 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=5.637 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=4.992 ms
```
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=9.329 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=10.115 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=10.595 ms
```
🌞 **Configuration des VLANs**
- déclaration des VLANs sur le switch `sw1`
```
(config)# vlan 10
(config-vlan)# name admins
(config-vlan)# exit

(config)# vlan 20
(config-vlan)# name guests
(config-vlan)# exit

(config)# exit
```
- ajout des ports du switches dans le bon VLAN 
  - ici, tous les ports sont en mode *access* : ils pointent vers des clients du réseau
```
# conf t
(config)# interface fastEthernet0/0
(config-if)# switchport mode access
(config-if)# switchport access vlan 10
(config-if)# exit
```
```
# conf t
(config)# interface fastEthernet0/1
(config-if)# switchport mode access
(config-if)# switchport access vlan 10
(config-if)# exit
```
```
# conf t
(config)# interface fastEthernet0/2
(config-if)# switchport mode access
(config-if)# switchport access vlan 20
(config-if)# exit
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.374 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=5.694 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=11.565 ms
```
- `pc3` ne ping plus personne
```
PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

# III. Routing

Dans cette partie, on va donner un peu de sens aux VLANs :

- un pour les serveurs du réseau
  - on simulera ça avec un p'tit serveur web
- un pour les admins du réseau
- un pour les autres random clients du réseau

Cela dit, il faut que tout ce beau monde puisse se ping, au moins joindre le réseau des serveurs, pour accéder au super site-web.

**Bien que bloqué au niveau du switch à cause des VLANs, le trafic pourra passer d'un VLAN à l'autre grâce à un routeur.**

Il assurera son job de routeur traditionnel : router entre deux réseaux. Sauf qu'en plus, il gérera le changement de VLAN à la volée.

## 1. Topologie 3


## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp4`

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***

```
enable
conf t
interface Gi0/0
ip address 10.1.1.1 255.255.255.0
```
On modifie le nom de l'interface en fonction de celle qu'on veut modifier et on applique l'ip qui lui convient (PC1, PC2, adm1).
Ici pour PC1 ça donne : 
```
PC1> show ip all

NAME   IP/MASK              GATEWAY           MAC                DNS
PC1    10.1.1.1/24          10.1.1.254        00:50:79:66:68:00
```

🌞 **Configuration des VLANs**

- déclaration des VLANs sur le switch `sw1`
- ajout des ports du switches dans le bon VLAN
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau (un *switch* et un *routeur*)

Config des vlan : 
```
# conf t
(config)# vlan 11
(config-vlan)# name vlan11
(config-vlan)# exit
(config)# vlan 12
(config-vlan)# name vlan12
(config-vlan)# exit
(config)# vlan 13
(config-vlan)# name vlan13
(config-vlan)# exit
```
Pour ajouter les ports de sw1 au bon Vlan : 
```
# conf t
(config)# interface fastEthernet0/0
(config-if)# switchport mode access
(config-if)# switchport access vlan 11
(config-if)# exit
```
On fait pareil en remplaçent l'interface réseau et l'id du vlan en fonction de la machine et hop les beaux vlan : 
```
Switch>show vlan

11   vlan11                           active    Gi0/0, Gi0/1
12   vlan12                           active    Gi0/3
13   vlan13                           active    Gi0/2
```
Pour le trunk : 
```
# conf t
(config)# interface Gi1/0
(config-if)# switchport trunk encapsulation dot1q
(config-if)# switchport mode trunk
(config-if)# switchport trunk allowed vlan add 11,12,13
(config-if)# exit
(config)# exit
```
Tout beau tout bien configuré : 
```
Switch>show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       1,11-13
```

---

➜ **Pour le *routeur***

- ici, on va avoir besoin d'un truc très courant pour un *routeur* : qu'il porte plusieurs IP sur une unique interface
  - avec Cisco, on crée des "sous-interfaces" sur une interface
  - et on attribue une IP à chacune de ces sous-interfaces
- en plus de ça, il faudra l'informer que, pour chaque interface, elle doit être dans un VLAN spécifique

🌞 **Config du *routeur***

- attribuez ses IPs au *routeur*
  - 3 sous-interfaces, chacune avec son IP et un VLAN associé

```cisco
# conf t

(config)# interface fastEthernet 0/0.11
R1(config-subif)# encapsulation dot1Q 11
R1(config-subif)# ip addr 10.1.1.254 255.255.255.0 
R1(config-subif)# exit

(config)# interface fastEthernet 0/0.12
R1(config-subif)# encapsulation dot1Q 12
R1(config-subif)# ip addr 10.2.2.254 255.255.255.0 
R1(config-subif)# exit

(config)# interface fastEthernet 0/0.13
R1(config-subif)# encapsulation dot1Q 13
R1(config-subif)# ip addr 10.3.3.254 255.255.255.0 
R1(config-subif)# exit
```

Les sous interfaces : 

```
R1#show running-config
[...]
!
interface FastEthernet0/0.11
 encapsulation dot1Q 11
 ip address 10.1.1.254 255.255.255.0
!
interface FastEthernet0/0.12
 encapsulation dot1Q 12
 ip address 10.2.2.254 255.255.255.0
!
interface FastEthernet0/0.13
 encapsulation dot1Q 13
 ip address 10.3.3.254 255.255.255.0

```

🌞 **Vérif**

Quelques test : 
```
PC1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=38.424 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=37.430 ms
84 bytes from 10.2.2.1 icmp_seq=3 ttl=63 time=39.232 ms
```

Gros soucis avec web1, impossible de ping la passerelle (donc les autres machines).

Config de web1
```
[dodo@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s3
UUID=7da91c7f-cc82-469f-aff9-99ed8a129376
DEVICE=enp0s3
ONBOOT=yes
IPADDR=10.3.3.1
NETMASK=255.255.255.0
DNS1=1.1.1.1
```
```
[dodo@web1 ~]$ sudo cat /etc/sysconfig/network
[sudo] password for dodo:
# Created by anaconda
GATEWAY=10.3.3.254
```
```
[dodo@web1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a9:a4:d0 brd ff:ff:ff:ff:ff:ff
    inet 10.3.3.1/24 brd 10.3.3.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea9:a4d0/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:df:d0:55 brd ff:ff:ff:ff:ff:ff
    inet 10.102.1.12/24 brd 10.102.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fedf:d055/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
Test ping passerelle : 
```
[dodo@web1 ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
From 10.102.1.12 icmp_seq=1 Destination Host Unreachable
From 10.102.1.12 icmp_seq=2 Destination Host Unreachable
From 10.102.1.12 icmp_seq=3 Destination Host Unreachable
--- 10.3.3.254 ping statistics ---
4 packets transmitted, 0 received, +3 errors, 100% packet loss, time 3100ms
pipe 4
```
Config routeur : 
```
R1#show ip route
Codes: C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route

Gateway of last resort is not set

     10.0.0.0/24 is subnetted, 3 subnets
C       10.3.3.0 is directly connected, FastEthernet0/0.13
C       10.2.2.0 is directly connected, FastEthernet0/0.12
C       10.1.1.0 is directly connected, FastEthernet0/0.11
```

Config vlan : 

```
Switch>show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi1/1, Gi1/2, Gi1/3, Gi2/0
                                                Gi2/1, Gi2/2, Gi2/3, Gi3/0
                                                Gi3/1, Gi3/2, Gi3/3
11   vlan11                           active    Gi0/0, Gi0/1
12   vlan12                           active    Gi0/3
13   vlan13                           active    Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```
```
Switch>show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       1,11-13
```
Test ping PC1 vers adm1 : 
```
PC1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=41.134 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=34.894 ms
84 bytes from 10.2.2.1 icmp_seq=3 ttl=63 time=26.615 ms
```
Test ping PC2 vers la passerelle web1 : 
```
PC2> ping 10.3.3.254

84 bytes from 10.3.3.254 icmp_seq=1 ttl=255 time=17.898 ms
84 bytes from 10.3.3.254 icmp_seq=2 ttl=255 time=15.016 ms
84 bytes from 10.3.3.254 icmp_seq=3 ttl=255 time=15.412 ms
```
Test ping PC2 vers web1 : 
```
PC2> ping 10.3.3.1

10.3.3.1 icmp_seq=1 timeout
10.3.3.1 icmp_seq=2 timeout
10.3.3.1 icmp_seq=3 timeout
```

A la place de web1 je test de mettre un VPCS PC3 dans le réseau 10.3.3.0/24 soit le vlan 13 : 
IP :
```
NAME   IP/MASK              GATEWAY           MAC                DNS
PC3    10.3.3.2/24          10.3.3.254        00:50:79:66:68:03  8.8.8.8
```
Je le rajoute au vlan 13 (interface Gi1/1 du switch) : 
```
Switch>show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi1/2, Gi1/3, Gi2/0, Gi2/1
                                                Gi2/2, Gi2/3, Gi3/0, Gi3/1
                                                Gi3/2, Gi3/3
11   vlan11                           active    Gi0/0, Gi0/1
12   vlan12                           active    Gi0/3
13   vlan13                           active    Gi0/2, Gi1/1
[...]
```
Je test de ping la passerelle 10.3.3.254 avec PC3 : 
```
PC3> ping 10.3.3.254

84 bytes from 10.3.3.254 icmp_seq=1 ttl=255 time=8.592 ms
84 bytes from 10.3.3.254 icmp_seq=2 ttl=255 time=12.764 ms
84 bytes from 10.3.3.254 icmp_seq=3 ttl=255 time=13.001 ms
```

Maintenant je test un ping vers PC3 depuis PC1 : 
```
PC1> ping 10.3.3.2

84 bytes from 10.3.3.2 icmp_seq=1 ttl=63 time=23.201 ms
84 bytes from 10.3.3.2 icmp_seq=2 ttl=63 time=25.440 ms
84 bytes from 10.3.3.2 icmp_seq=3 ttl=63 time=23.116 ms
```
Le ping fonctionne, le problème viens donc de la VM mais impossible de le résoudre(même après avoir refait une VM depuis le patron rocky).

# IV. NAT

On va ajouter une fonctionnalité au routeur : le NAT.

On va le connecter à internet (simulation du fait d'avoir une IP publique) et il va faire du NAT pour permettre à toutes les machines du réseau d'avoir un accès internet.

## 1. Topologie 4

## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `pc3.servers.tp4`  | x               | x               | `10.3.3.2/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

J'utiliserai pc3 à la place de web1 au vu du problème rencontré plus haut.

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

- branchez à `eth1` côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP

```
# conf t
(config)# interface fastEthernet1/0
(config-if)# ip address dhcp
(config-if)# no shut
```
```
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.11         10.1.1.254      YES NVRAM  up                    up
FastEthernet0/0.12         10.2.2.254      YES NVRAM  up                    up
FastEthernet0/0.13         10.3.3.254      YES NVRAM  up                    up
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
FastEthernet3/0            unassigned      YES NVRAM  administratively down down
NVI0                       unassigned      NO  unset  up                    up
```

- vous devriez pouvoir `ping 1.1.1.1`
C'est good !
```
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 64/67/72 ms

```

🌞 **Configurez le NAT**

```
# conf t
(config)# interface fastEthernet 0/0
(config-if)# ip nat outside
(config-if)# exit
(config)# interface fastEthernet 1/0
(config-if)# ip nat inside
(config-if)# exit
(config)# access-list 1 permit any
(config)# ip nat inside source list 1 interface fastEthernet 0/0 overload
```

🌞 **Test**

Pour le DNS : 
```
PC1> ip dns 8.8.8.8
```
Et hop : 
```
PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 10.1.1.254
DNS         : 8.8.8.8
MAC         : 00:50:79:66:68:00
LPORT       : 20011
RHOST:PORT  : 127.0.0.1:20012
MTU         : 1500
```
On fait de même sur les autres VPCS.
Et pour la VM : 
```
[dodo@web1 ~]$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3
```
Et on rajoute cette ligne : 
```
DNS1=1.1.1.1
```
Mais j'ai toujours le problème avec la passerelle, donc ping vers google.com impossible.
J'utilise donc PC3 à la place : 
```
PC3> ping google.com
google.com resolved to 216.58.213.174

84 bytes from 216.58.213.174 icmp_seq=1 ttl=113 time=28.697 ms
84 bytes from 216.58.213.174 icmp_seq=2 ttl=113 time=31.271 ms
84 bytes from 216.58.213.174 icmp_seq=3 ttl=113 time=29.985 ms
```
Fonctionne aussi sur les autres machines comme adm1 : 
```
adm1> ping google.com
google.com resolved to 142.250.179.110

84 bytes from 142.250.179.110 icmp_seq=1 ttl=114 time=30.476 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=114 time=31.992 ms
84 bytes from 142.250.179.110 icmp_seq=3 ttl=114 time=37.274 ms
```
