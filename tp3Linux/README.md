# TP3 : Your own shiet

# Intro

**Dans ce TP, vous allez installer une solution de votre choix.** Pas la développer, mais bien installer et configurer un truc existant.

Pensez large :

- hébergement de fichiers
- streaming audio/vidéo
- webradio
- héberger votre propre dépôt git
- serveur de jeu
- serveur VPN
- etc.

La solution doit être un projet libre et open-source (souvent, le code et la doc seront accessibles sur GitHub).


# Documentation

Le but du projet est d'avoir deux machines pour entretenir un parc de serveur Minecraft. La première machine hébergera les serveurs, un service de monitoring ainsi qu'un service de backup. La deuxième machine servira de récepteur pour les backups via un partage Nfs.

## Setup des machines 

Ici on utilisera des VM avec rocky Linux.

### Checklist : 

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] accès Internet

| Machine         | IP            | Port ouvert / service autorisé |
|-----------------|---------------|-------------|
| `serveur.tp3.Linux` | `10.102.1.2/24` | ssh, 19999/tcp, 25565/tcp, 25565/udp      |
| `backups.tp3.Linux` | `10.102.1.3/24` | ssh          |

On commence par mettre à jour la machine : 
```
sudo dnf update
```
Pour modifier le nom de la machine : 
```
sudo hostname "nom_de_la_machine"
```
exemple
```
sudo hostname "serveur.tp3.Linux"
```
Et inscrire le nom dans le fichier /etc/hostname : 
```
sudo vi /etc/hostname
```
Pour modifier l'ip de la machine, modifier le fichier de conf de la carte de votre machine : 
```
sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8
```
Incrivez ceci dedans : 
```
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.102.1.3
NETMASK=255.255.255.0
DNS1=8.8.8.8
```
Changez cette ligne : ```IPADDR=10.102.1.3``` avec l'ip adapté à chaque machine.
Et on en profite pour rajouter un DNS par défaut.

Pour le firewall, on enlève simplement ce qui ne nous sert pas, et on rejoutera ce qui nous servira. Pour l'instant on ne va utiliser que ssh.

Pour supprimer un service dans le firewall : 
```
sudo firewall-cmd --remove-service=nom_du_service
```
```
sudo firewall-cmd --remove-service=nom_du_service --permanent
```

exemple si on voulait enlever ssh : 
```
sudo firewall-cmd --remove-service=ssh
sudo firewall-cmd --remove-service=ssh --permanent
```

Sur mes VM l'accès internet ce fait via une carte NAT.

### Partage Nfs : 

Pour mettre en place les backups (pour les serveurs Minecraft) il va falloir faire un partage de fichier (ou partage nfs).

Sur la machine Backups : 
* créer le dossier /srv/backups/web.tp2.linux/
* tapez ```sudo dnf -y install nfs-utils```
* tapez ```sudo vi /etc/idmapd.conf```
* enlever le commentaire à la ligne "Domain =" et inscrivez à la suite du égale le domaine de votre machine. Sauvegardez et fermez.
* tapez ```vi /etc/exports``` 
* dans ce fichier inscivez "/srv/backups/web.tp2.linux/ 10.0.0.0/24(rw,no_root_squash)", mais remplacez l'ip et le masque par ceux de votre machine. Sauvegardez et fermez.
* enfin tapez ```systemctl enable --now rpcbind nfs-server```

Pensez à ajouter le service dans le firewall.
On a fini du côté de la machine backups, on va passer a la configuration sur la machine serveur.

Sur la machine Serveur : 
* créer le dossier /srv/backup/
* tapez ```sudo dnf -y install nfs-utils```
* tapez ```sudo vi /etc/idmapd.conf```
* enlever le commentaire à la ligne "Domain =" et inscrivez à la suite du égale le domaine de votre machine. Sauvegardez et fermez.
* pour que la comande suivante fonctionne, il faut que votre machine Serveur puisse résoudre le nom de votre machine Backups. Si ce n'est pas le cas, remplacez le nom de domaine par l'ip de la machine backups : 
	* tapez ```mount -t nfs backups.tp3.linux:/srv/backups/web.tp2.linux/ /srv/backup/``` 
	* vérifiez que ça fonctionne avec ```df -hT```, les dossiers de la commande précédente devraient apparaitre. 
* et pour que le partage nfs de lance au démarage de la machine, tapez ```vi /etc/fstab```
* dans ce fichier inscrivez "backups.tp3.linux:/srv/backups/web.tp2.linux/ /srv/backup/nfs     defaults        0 0"

Et voilà pour la configuration du partage sur les machines.

### SSH : 

Puisque l'objectif est de pouvoir déployer les serveurs facilement et rapidement, on va faciliter ça avec un échange de clefs ssh entre les machines de travail et les machines du projet.

On commence donc par créer les clefs sur la machine de travail : 
```
ssh-keygen -t rsa -b 4096
```
Accepter l’emplacement par défaut pour la sauvegarde de la clé en appuyant sur entrée. Vous pouvez ensuite choisir le mot de passe pour utiliser la clef. La génération peut prendre un peu de temps.

Pour vérifier la bonne génération, sur Linux, vous pouvez taper : 
```
[dodo@admin ~]$ tree -a .ssh/
```
Vous devriez au moins obtenir ceci : 
```
├── id_rsa
└── id_rsa.pub
```

Sur Windows rendez vous ici : 

```
C:\Users\username\.ssh
```
Vous devriez au moins obtenir ceci : 
```
id_rsa
id_rsa.pub
```

id_rsa  est notre clef privée et id rsa pub est notre clef publique, c'est d'ailleurs cette dernière qui nous intéresse désormais. On doit la transférer vers nos machines serveur et backups. Il y a différentes méthodes, mais pour ma part je vais simplement copier coller la clef dans le fichier ```~/.ssh/authorized_keys```. Tapez :

```
[dodo@admin ~]$ cd
[dodo@admin ~]$ mkdir .ssh
[dodo@admin ~]$ cd .ssh
[dodo@admin ~]$ vim authorized_keys
```
Il vous suffit ensuite de coller la clef publique dans le fichier.
On va maintenant s'occuper des permissions. Deux commandes et c'est réglé :
```
[dodo@admin ~]$ chmod 600 /home/user/.ssh/authorized_keys
[dodo@admin ~]$ chmod 700 /home/user/.ssh
```
Il ne vous reste plus qu'à redémarrer votre service ssh et vous devriez pouvoir vous connecter sans mot de passe depuis votre machine perso vers celles du projet. Désormais si vous le souhaitez, vous pouvez désactiver la connexion ssh avec mot de passe. Elle sera donc uniquement accessible à ceux qui y auront déposé une clé publique (Pensez que si votre échange ne fonctionne plus il vous faudra un accès physique à la machine pour le restaurer). Ce site vous explique comment faire : 
https://www.jtouzi.net/asides/desactiver-lauthentification-ssh-par-mot-de-passe/

### Java : 

Pour démarer un serveur Minecraft vous aurez besoin de java, pour l'installer sur votre machine Serveur, une seule commande : 
```
sudo dnf install java
```

### Mcrcon : 
Enfin, pour manipuler vos serveurs vous aurez besoin d'un client rcon sur la machine serveur. Personellement j'utilise celui-ci qui est facile d'utilisation et simple d'installation : 
* https://github.com/Tiiffi/mcrcon

Vous aurez très probablement besoin d'installer ces 3 choses : 
* git
* gcc
* make


## Le Projet 

Maintenant que les machines sont correctement configurées, on va importer les scripts des serveurs Minecraft et mettre en place l'arborescence du projet. Ils serviront à démarrer un serveur, en créer un nouveau, changer la configuration,...

Le principe est celui-ci : 
* Un dossier principal ```ProjetLinuxtp3ServMc```.
* Un preset des serveurs que l'on copiera pour en créer de nouveau grâce à un script.
* Un dossier ```Serveurs``` dans lequel on mettra tous les serveurs.
* Le fichier ```minecraft_server.1.15.2.jar```qui sert à démarer un serveur grâce aux fichiers de configurations et que l'on mettra dans le dossier principal.
* Les scripts de manipulations des serveurs que l'on laissera eux aussi dans le dossier principal.

Cela donne a peut prêt ceci : 
```
[dodo@serveur ~]$ tree ProjetLinuxtp3ServMc/
ProjetLinuxtp3ServMc/
├── backup.sh
├── delete.sh
├── install.sh
├── mcrcon
├── minecraft_server.1.15.2.jar
├── preset
├── rconlist.sh
├── send.sh
├── Serveurs
├── servport.sh
├── start.sh
└── stop.sh
```
Vous trouverez tout cet arborescence [ici](/tp3Linux/ProjetLinuxtp3ServMc). Il ne vous reste plus qu'à tout importer sur votre machine.

Commencez par utiliser le script install.sh, c'est lui qui permet de créer un nouveau serveur, exemple : 
```
[dodo@serveur ProjetLinuxtp3ServMc]$ ./install.sh test
```

Pensez à modifier les ports des nouveaux serveurs que vous créés. Cette modification se fait dans le dossier de configuration du serveur qui se trouve dans le dossier ```Serveurs/```. Prenons le serveur ```test``` en exemple, une fois dedans vous devriez avoir un fichier ```server.properties``` : 
```
[dodo@serveur test]$ sudo vi server.properties
```
Dans vi tapez ```/server-port```, vous devriez être sur la ligne de configuration du port du serveur, changez donc le port 25565 par celui qui vous convient et assurez vous qu'il ne soit pas deja utilisé.

De la même façon, il faut changer le port de mcrcon, et lui attribuer un mot de passe. Toujours dans vi, tapez : 
* ```/rcon.port``` : ici changez le port par défaut (25575) pour un un autre et assurez vous qu'il ne soit pas deja utilisé.
* ```/rcon.password``` : ici vous pouvez changer le mot de passe de connection a rcon, mais si vous le faites pensez aussi a le changer dans les requetes rcon des scripts. Le mot de passe actuel de rcon est admin1234.

Ensuite pensez à ouvrir les ports du serveur que vous venez de créer, pour le port du serveur, par défaut il s'agit du port 25565. Pour l'ouvrir : 
```
sudo firewall-cmd --add-port=25565/tcp
sudo firewall-cmd --add-port=25565/tcp --permanent
sudo firewall-cmd --add-port=25565/udp
sudo firewall-cmd --add-port=25565/udp --permanent
```

Si vous voulez trouver tous les ports utilisés pas vos serveurs, utilisez les scripts servport.sh et rconlist.sh, exemples : 
```
[dodo@serveur ProjetLinuxtp3ServMc]$ ./rconlist.sh
test : 25575
```
```
[dodo@serveur ProjetLinuxtp3ServMc]$ ./servport.sh
test : 25565
```
Désormais on sait quels ports server et rcon sont utilisés par le serveur Minecraft test.

Enfin, il ne reste plus qu'a utilisé le script ./start.sh suivi du nom du serveur que vous souhaitez démarer :
```
./start.sh test
```
Vous pouvez éteindre le serveur avec le script stop.sh suivi du nom du serveur, exemple : 
```
./stop.sh test
```

Si un serveur est inutilisé vous pouvez utiliser le script delete.sh : 
```
[dodo@serveur ProjetLinuxtp3ServMc]$ ./delete.sh test
Connection failed.
Error 111: Connection refused
Are you sure ? [Yes:No]
Yes
```
Les lignes 2 et 3 ne sont pas bug, l'erreur viens du fait que le script essaye de se connecter au serveur pour verifier qu'il n'est pas en fonctionnement. Etant donné qu'il ne l'est pas, il ne peut pas se connecter.
Suite a la commande le serveur a bien disparu des fichiers.
```
[dodo@serveur ProjetLinuxtp3ServMc]$ ls Serveurs/
[dodo@serveur ProjetLinuxtp3ServMc]$
```
Enfin si vous voulez envoyez des commandes sur votre serveur minecraft sans vous connecter dessus, le script send.sh est fait pour vous : 
```
[dodo@serveur ProjetLinuxtp3ServMc]$ ./send.sh test "say hello world !"
```
Le premier argument est le nom du serveur et le second la commande Minecraft que voud taperiez dans le tchat de jeu normalement. Pensez bien a mettre cette commande entre double quote.
Rendu dans les logs : 
```
[12:40:14] [RCON Listener #2/INFO]: Rcon connection from: /0:0:0:0:0:0:0:1
[12:40:14] [Server thread/INFO]: [Rcon] hello world !
```

Pour vous connecter au Serveur, lancez votre jeu, rendez vous dans l'onglet multijoueur et ajoutez un nouveau server avec l'ip et le port du serveur sur la machine: 

![image1](/tp3Linux/pics/image server connect.png)

La commande passé plus haut avec send.sh apparait aussi sur le serveur : 

![image2](/tp3Linux/pics/image server rcon.png)

## Maintien en condition opérationnelle

### Netdata : 

Netdata est un service de monitoring, il va vous permettre de facilement garder un oeil sur les permformances de votre machine serveur. Si vous voulez plus d'infos sur le service, leur site est juste ici : https://learn.netdata.cloud/docs/overview/what-is-netdata.

Ce que l'on va faire pour le projet, c'est instaler la solution et la configurer pour acceder au dashboard via une page web. Commençons par l'installation : 
Pensez a update votre machine si vous ne l'avez pas fait depuis longtemps.
```
[dodo@serveur ProjetLinuxtp3ServMc]$ sudo su -
[dodo@serveur ProjetLinuxtp3ServMc]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[dodo@serveur ProjetLinuxtp3ServMc]$ exit
```
Netdata devrait être installé et démarrer, s'il ne l'est pas :
```
 sudo systemctl start netdata
```
Il faut maintenant ouvrir le port qu'il utilise. Vous pouvez le trouver avec une commande ss, mais par défaut le port devrait être 19999. Et pour l'ouvrir c'est comme avec les ports des serveurs Minecraft : 
```
sudo firewall-cmd --add-port=19999/tcp
sudo firewall-cmd --add-port=19999/tcp --permanent
```
Tout devrait être prêt pour le monitoring, il ne vous reste plus qu'à ouvrir un navigateur internet et à vous rendre sur l'ip de votre machine suivi du port. Vous devriez arriver sur une interface similaire à ça : 

![image3](/tp3Linux/pics/image netdata.png)

### Backups 

Enfin pour terminer ce projet, nous allons mettre en place un service de backup avec un timer. Grâce à ça nous pourrons démarer le service avec une commande systemd. Et cela permettra aussi au système de backup d'automatiquement démarer au boot de la machine.
On commence donc par faire un service avec notre script, il faut créer un fichier .service ici : ```/etc/systemd/system/```. Tapez :
```
[dodo@serveur system]$ sudo vi backupServeur.service
```
Dans ce fichier, copier coller ceci : 
```
[Unit]
Description=Service de backup de serveurs Minecraft

[Service]
ExecStart=/home/dodo/ProjetLinuxtp3ServMc/backup.sh
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```
Ensuite, ouvrez le fichier `backup.sh`, à la ligne 10, et modifiez : 
* ceci ```../../home/dodo/ProjetLinuxtp3ServMc/Serveurs/```
* par la direction vers le dossier `Serveurs/` depuis `/srv/backup/` de votre machine. Pour l'instant il s'agit de la direction vers `/home/dodo`, donc mon user, il faut que vous adaptiez cette direction à votre user.

Et c'est fini pour le service. Il ne reste que deux commandes à taper, une pour prendre en compte les modifications apportés et une autre pour démarrer le service. C'est à vous : 
```
[dodo@serveur system]$ sudo systemctl daemon-reload
```
```
[dodo@serveur system]$ sudo systemctl start backupServeur
```
Si vous voulez vérifier que ça a fonctionné, vous pouvez tapez : 
```
[dodo@serveur system]$ sudo systemctl status backupServeur.service
```
Vous devriez obtenir quelque chose du genre : 
```
● backupServeur.service - Service de backup de serveurs Minecraft
   Loaded: loaded (/etc/systemd/system/backupServeur.service; disabled; vendor preset: disabled)
   Active: inactive (dead)

Nov 14 19:32:07 serveur.tp3.linux systemd[1]: Starting Service de backup de serveurs Minecraft...
Nov 14 19:32:07 serveur.tp3.linux backup.sh[5758]: TERM environment variable not set.
Nov 14 19:32:09 serveur.tp3.linux backup.sh[5758]: tar: */2021-11-14-19\:32h: Cannot stat: No such file or directory
Nov 14 19:32:09 serveur.tp3.linux backup.sh[5758]: tar (child): Serveurs/: Cannot open: Is a directory
Nov 14 19:32:09 serveur.tp3.linux backup.sh[5758]: tar (child): Error is not recoverable: exiting now
Nov 14 19:32:09 serveur.tp3.linux backup.sh[5758]: tar: Child returned status 2
Nov 14 19:32:09 serveur.tp3.linux backup.sh[5758]: tar: Error is not recoverable: exiting now
Nov 14 19:32:09 serveur.tp3.linux backup.sh[5758]: Backup complete !
Nov 14 19:32:09 serveur.tp3.linux systemd[1]: backupServeur.service: Succeeded.
Nov 14 19:32:09 serveur.tp3.linux systemd[1]: Started Service de backup de serveurs Minecraft.
```
Ici tout à bien fonctionné ! Mais comme je l'ai dit plus haut on va aller un peu plus loin en ajoutant un timer, un systeme qui va permettre d'éxécuter notre service tout les `x` temps.

On va donc créer encore une fois un fichier, avec le même nom mais pas la même extension, c'est grâce a ça que systemd gère les deux. Vous pouvez donc créer le fichier avec cette commande et toujours au même endroit : 
```
[dodo@serveur system]$ sudo vi backupServeur.timer
```
Une fois dedans copier coller ça : 
```
[Unit]
Description=Exécute périodiquement le service de backup
Requires=backupServeur.service

[Timer]
Unit=backupServeur.service
OnCalendar=*-*-* *:00:00

[Install]
WantedBy=timers.target
```

La ligne OnCalendar fonctionne ainsi : `Année-Mois-Jour Heures:Minutes:Secondes`
En laissant comme je l'ai fait votre timer s'exécutera tout les ans, tous les mois, tous les jours et toutes les heures à 0 minutes et 0 secondes. Soit chaques heures.

Encore une fois il ne reste que deux commandes a faire : 
```
sudo systemctl start backupServeur.timer
sudo systemctl enable backupServeur.timer
```

Enfin si vous voulez verifier quand se fera la prochaine backup, vous pouvez essayer : 
```
[dodo@serveur system]$ sudo systemctl status backupServeur.timer
● backupServeur.timer - Exécute périodiquement le service de backup
   Loaded: loaded (/etc/systemd/system/backupServeur.timer; disabled; vendor preset: disabled)
   Active: active (waiting) since Sun 2021-11-14 20:05:45 CET; 15s ago
  Trigger: Sun 2021-11-14 21:00:00 CET; 53min left

Nov 14 20:05:45 serveur.tp3.linux systemd[1]: Started Exécute périodiquement le service de backup.
```

On peut voir que le timer viens de démarer a 20:05 et qu'il fera une backup a 21h00. Exactement ce que l'on voulait.

Pour conclure, nous allons verifier si les backups sont bien envoyées vers la machine backups.
On test avec ```./backup.sh```.
Si tout fonctionne normalement, on devrait avoir le fichier .tar.gz dans le partage de fichier sur la machine backup.
On check dans ```/srv/backups/serveur.tp3.linux``` : 
```
[dodo@backups serveur.tp3.linux]$ ls -al
total 2648
drwxr-xr-x. 2 root root      37 Nov 14 21:36 .
drwxr-xr-x. 3 root root      31 Oct 28 16:26 ..
-rw-r--r--. 1 root root 2709881 Nov 14 21:33 2021-11-14-21h33.tar.gz
```
On peut voir l'archive avec l'heure dans le partage de fichier, le projet a atteint ses objectifs.

Ce projet est donc terminé ! Vous êtes libre de créer de nouveaux scripts, nouveaux services ou autres. J'espère que les explications étaient claires.
