#!/bin/bash
#FONSECA DORIAN, 14/11/2021
#Script to see all minecraft servers ports

cd Serveurs/
for d in *
do
	cd $d
	if [ -f "server.properties" ]
	then
		port=$(grep server-port server.properties | cut -d'=' -f2)
		cd ..
		echo "$d : $port"
	else
		cd ..
		continue
	fi
done

