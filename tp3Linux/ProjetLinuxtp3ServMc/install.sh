#!/bin/bash
#FONSECA DORIAN, 14/11/2021
#Script to create a minecraft server

clear

name="$1"

if [ $# = 1 ]
then
	cd Serveurs/
	if [ -d $1 ]
	then
		echo 'Server already exist'
		exit
	else
		cd ..
	fi
        if [ -d $pwd ] && [ -r $pwd ] && [ -w $pwd ]
        then
		cp -r preset/ Serveurs/
		cd Serveurs/
		mv preset $1
		cd ..
		cd ..
            else
                echo "Check access rights to the folder"
                exit
        fi
    elif [[ $# > 1 ]]
    then
        echo "Too much arguments"
        exit
    elif [[ $# < 1 ]]
    then
        echo "Missing arguments"
        exit
fi

