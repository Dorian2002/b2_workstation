#!/bin/bash
#FONSECA DORIAN, 14/11/2021
#Script to start a minecraft server

clear

if [ $# = 1 ]
then
	cd Serveurs
	if [ -d $1 ]
	then
        	cd $1
		clear
		java -Xmx512M -Xms512M -jar ../../minecraft_server.1.15.2.jar nogui &
		echo "Server is Starting !"
		exit
	else
		echo 'Server does not exist'
		exit
	fi
elif [[ $# > 1 ]]
then
	echo "Too much arguments"
	exit
elif [[ $# < 1 ]]
then
	echo "Missing arguments"
	exit
fi

