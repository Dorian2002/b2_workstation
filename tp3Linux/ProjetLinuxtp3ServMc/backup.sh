#!/bin/bash
#FONSECA DORIAN, 14/11/2021
#Script de backup de serveur Minecraft

clear
if [ $# = 0 ]
then
	echo "test1"
	cd /srv/backup/
	tar -cvzf serveur.tar.gz ../../home/dodo/ProjetLinuxtp3ServMc/Serveurs/
	mv serveur.tar.gz $(date +%Y-%m-%d-%Hh%M).tar.gz
	echo 'Backup complete !'
	echo ""

elif [ $# > 0 ]
then
        echo "Too much arguments"
        exit
fi
