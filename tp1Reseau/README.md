# B2_TP1_Réseau

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

🌞 Affichez les infos des cartes réseau de votre PC
🌞 Affichez votre gateway

j'utilise la commande ```ipconfig```
* interface WiFi : 
    ```
    Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::b480:a6b7:6412:8f59%6
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.236
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   ```
   On peut aussi voir la gateway avec cette commande.
   
* interface Ethernet : 
    ```
    Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
    ```

🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

Avec l'interface graphique windows, je fait clique droit sur le bouton réseau/wifi du bureau, puis ```Ouvrir les paramètres réseau et internet``` et enfin ```Afficher les propriétés du materiel et de la connexion```.
remplacer par le screen local
![](https://i.imgur.com/ryCaiL7.png)

🌞 à quoi sert la gateway dans le réseau d'YNOV ?

La passerelle du réseau Ynov permet au utilisateurs interne au réseau d'acceder aux réseaux externes.

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP : 
remplacer par le screen local
![](https://i.imgur.com/GUW32ze.png)

🌞 Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

Il est possible que l'IP rentrée manuellement soit deja utilisée.

#### B. Table ARP

🌞 Exploration de la table ARP

```
arp -a

Interface : 10.33.2.236 --- 0x6
  Adresse Internet      Adresse physique      Type
  10.33.2.174           5c-80-b6-f7-59-5e     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.254.1 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.254.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.142.1 --- 0xc
  Adresse Internet      Adresse physique      Type
  192.168.142.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.10.2 --- 0xe
  Adresse Internet      Adresse physique      Type
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.10.1 --- 0x1b
  Adresse Internet      Adresse physique      Type
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.120.1 --- 0x1c
  Adresse Internet      Adresse physique      Type
  192.168.120.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

L'adresse mac de la passerelle du réseau Ynov est ```00-12-00-40-4c-bf```. Car l'ip correspond à l'une des dernières du réseau et est dynamique.

🌞 Et si on remplissait un peu la table ?

j'utilise la commande ```ping``` en direction des IP ci dessous :-1: 

```
10.33.3.13
10.33.3.109
10.33.3.112 
```
On trouve dans la table ARP les IP vers lesquels les ping ont été envoyés :
```
arp -a

Adresse Internet      Adresse physique
10.33.3.13            26-7b-3f-46-6d-9e
10.33.3.109           38-f9-d3-2f-c2-79
10.33.3.112           3c-06-30-2d-48-0d
```
(j'ai volontairement enlevé le type car inutile ici)


#### C. nmap

🌞Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

```
nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-15 13:38 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.31
Host is up (0.029s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
Nmap scan report for 10.33.0.38
Host is up (0.11s latency).
MAC Address: 84:1B:77:F9:21:71 (Intel Corporate)
Nmap scan report for 10.33.0.41
Host is up (0.23s latency).
MAC Address: B0:EB:57:82:54:35 (Huawei Technologies)
Nmap scan report for 10.33.0.71
Host is up (0.32s latency).
MAC Address: F0:03:8C:35:FE:47 (AzureWave Technology)
Nmap scan report for 10.33.0.85
...
```
On peut voir toutes les IP utilisés du réseau Ynov.
L'adresse IP ```10.33.0.73``` est non utilisée sur le réseau Ynov par exemple.

Suite au scan on peut voir que la table ARP c'est bien rempli.
```
Interface : 10.33.2.236 --- 0x6
  Adresse Internet      Adresse physique      Type
  10.33.0.3             22-93-d2-7e-9f-1a     dynamique
  10.33.0.10            b0-6f-e0-4c-cf-ea     dynamique
  10.33.0.31            fa-c5-6d-60-4b-4c     dynamique
  10.33.0.41            b0-eb-57-82-54-35     dynamique
  10.33.0.71            f0-03-8c-35-fe-47     dynamique
  10.33.0.124           36-98-05-f5-ee-5a     dynamique
  10.33.0.164           9a-75-ee-ef-e2-00     dynamique
  10.33.0.174           40-ec-99-c6-dc-c5     dynamique
  10.33.0.175           74-d8-3e-f2-cc-95     dynamique
  10.33.0.234           c8-58-c0-27-22-fc     dynamique
  10.33.0.243           dc-21-5c-3b-a3-f3     dynamique
  10.33.1.48            34-42-62-23-f7-3c     dynamique
  10.33.1.69            a0-a4-c5-41-bf-4f     dynamique
  10.33.1.101           e0-cc-f8-7d-b8-fc     dynamique
  10.33.1.112           9c-fc-e8-b8-0e-02     dynamique
  10.33.1.118           a2-24-4b-f2-bf-6a     dynamique
  10.33.1.146           0c-54-15-44-1a-56     dynamique
  10.33.1.148           4a-c8-d7-aa-c1-d4     dynamique
  10.33.1.172           50-e0-85-db-61-8b     dynamique
  10.33.1.182           5e-1d-0e-63-78-34     dynamique
  10.33.1.192           2c-8d-b1-29-33-c6     dynamique
  10.33.2.16            70-66-55-8b-df-07     dynamique
  10.33.2.32            d8-ce-3a-da-c4-64     dynamique
  10.33.2.61            88-66-5a-51-c5-89     dynamique
  10.33.2.145           52-80-48-e2-04-cf     dynamique
  10.33.2.149           82-2d-4a-c7-24-c4     dynamique
  10.33.2.159           32-b9-7f-73-b4-23     dynamique
  10.33.2.160           de-e6-48-af-e5-d2     dynamique
  10.33.2.210           e8-5a-8b-36-99-15     dynamique
  10.33.2.241           9c-6b-72-9b-aa-8b     dynamique
  10.33.3.17            80-32-53-e2-78-04     dynamique
  10.33.3.23            7e-7e-4e-bb-23-88     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.86            a6-1a-95-1d-2c-fe     dynamique
  10.33.3.97            c6-20-1c-b0-0d-47     dynamique
  10.33.3.101           72-85-b9-a6-4e-d2     dynamique
  10.33.3.141           6e-54-2e-39-b3-4b     dynamique
  10.33.3.156           04-d6-aa-b6-f5-f2     dynamique
  10.33.3.168           12-88-c1-71-b7-a0     dynamique
  10.33.3.171           ba-a8-7c-d3-2e-74     dynamique
  10.33.3.194           58-96-1d-db-e5-6b     dynamique
  10.33.3.195           e0-1f-88-eb-12-1d     dynamique
  10.33.3.201           82-74-57-ee-b9-59     dynamique
  10.33.3.224           ce-76-55-bf-50-22     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  ...
```

#### D. Modification d'adresse IP (part 2)

🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap : 

![](https://i.imgur.com/Hcl5LoN.png)

Vérification de l'accès à internet : 
```
PS C:\Users\User> ping 8.8.8.8
Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 19ms, Moyenne = 18ms
```

Puis avec la commande ```ipconfig``` on obtient : 
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::b480:a6b7:6412:8f59%6
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.98
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

## II. Exploration locale en duo

### 3. Modification d'adresse IP

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :
![](https://i.imgur.com/n1wBJEL.png)



Vérification des modifications : 
```
PS C:\Users\User> ipconfig
Configuration IP de Windows

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::7daa:5e8c:9826:5ec5%3
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.137.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.137.1
```

Ping de l'autres machines :
```
PS C:\Users\User> ping 192.168.137.1

Envoi d’une requête 'Ping'  192.168.137.1 avec 32 octets de données :
Réponse de 192.168.137.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.137.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
```

Table arp : 
```
PS C:\Users\User> arp -a
Interface : 192.168.137.23 --- 0x3
  Adresse Internet      Adresse physique      Type
  192.168.137.1         a8-5e-45-38-8d-fd     dynamique
  192.168.137.3         ff-ff-ff-ff-ff-ff     statique
```

### 4. Utilisation d'un des deux comme gateway

🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

```
PS C:\Users\User> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 19ms, Maximum = 23ms, Moyenne = 20ms
```
🌞 utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)
```
PS C:\Users\User> tracert 8.8.8.8

Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1    <1 ms     1 ms     1 ms  LAPTOP-6AO987H5 [192.168.137.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3    17 ms     7 ms    17 ms  10.33.3.253
```

### 5. Petit chat privé

🌞 sur le PC client
```
PS C:\Users\User\Desktop\netcat-1.11> .\nc.exe 192.168.137.1 8888
hey
salut
hi
```

🌞 pour aller un peu plus loin
(issu du PC de Fabio)
```
C:\Users\33785\Desktop\netcat>nc.exe -l -p 8888 192.168.137.6
invalid connection to [192.168.137.1] from (UNKNOWN) [192.168.137.23] 1054

C:\Users\33785\Desktop\netcat>nc.exe -l -p 8888 192.168.137.2
invalid connection to [192.168.137.1] from (UNKNOWN) [192.168.137.23] 1055

C:\Users\33785\Desktop\netcat>nc.exe -l -p 8888 192.168.137.23
hey
```

## 6. Firewall

🌞 Autoriser les ping

On autorise le ping grâce aux deux règles ci dessous : 
```
   PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.

PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V6 echo request" protocol=icmpv6:8,any dir=in action=allow
Ok.
```
On peut voir que la règle fonctionne : 
```
PS C:\Users\User\Desktop\netcat-1.11> ping 192.168.137.1

Envoi d’une requête 'Ping'  192.168.137.1 avec 32 octets de données :
Réponse de 192.168.137.1 : octets=32 temps=6 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.137.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 6ms, Moyenne = 1ms
```

On autorise le traffic pour netcat avec cette règle : 
```
netsh advfirewall firewall add rule name="Allow TCP on port 4242" dir=in action=allow protocol=TCP localport=4242
```
On peut voir que la règle fonctionne : 
```
PS C:\Users\User\Desktop\netcat-1.11> .\nc.exe 192.168.137.1 4242
hey
```
## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

🌞Exploration du DHCP, depuis votre PC

J'utilise ```ipconfig /all``` : 
```
Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
```
Avec cette même commande, je peut voir le bail DHCP : 
```
   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 15:26:36
   Bail expirant. . . . . . . . . . . . . : jeudi 16 septembre 2021 17:26:36
```
Le bail DHCP sur le réseau Ynov dure donc 2 heures.

## 2. DNS

🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

J'utilise ```ipconfig /all``` : 

```
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                            10.33.10.148
                                            10.33.10.155
```

🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

pour google.com :
```
PS C:\Users\User\Desktop\netcat-1.11> nslookup google.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:813::200e
          216.58.206.238
```
pour ynov.com :
```
PS C:\Users\User\Desktop\netcat-1.11> nslookup ynov.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

La commande nslookup a récupéré les IPs lié au DNS.

L'IP du serveur auquel on viens d'effectuer ces requêtes est 10.33.10.2, soit le serveur DNS d'Ynov, le réseau auquel on est connecté.

pour 78.74.21.21 :
```
PS C:\Users\User\Desktop\netcat-1.11> nslookup 78.74.21.21
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```
pour 92.146.54.88 :
```
PS C:\Users\User\Desktop\netcat-1.11> nslookup 92.146.54.88
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```

Le serveur DNS du réseau Ynov à récupéré un nom de domaine à partir de l'IP fournie.

## IV. Wireshark

🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence : 

Le ping :
![](https://i.imgur.com/lwLgnUy.png)

Le tchat netcat : 
![](https://i.imgur.com/d9RqVhq.png)

La requête DNS (à google) : 
![](https://i.imgur.com/qZjOzCK.png)