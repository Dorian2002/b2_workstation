# B2_TP2_Linux

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.102.1.0/24`.**

➜ Chaque **création de machines** sera indiqué par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel avec un échange de clé
- [x] accès Internet (une route par défaut, une carte NAT c'est très bien)
- [x] résolution de nom

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Un premier serveur web

## 1. Installation

🌞 **Installer le serveur Apache**
```
[dodo@web ~]$ sudo dnf install httpd -y
```
```
[dodo@web ~]$ sudo vi /etc/httpd/conf/httpd.conf
```
Dans vim `:g/^ *#.*/d`
```
[dodo@web conf]$ cat httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache

[...]

EnableSendfile on

IncludeOptional conf.d/*.conf
```

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
```
[dodo@web ~]$ sudo dnf update
```
  - démarrez le
    ```
    [dodo@web ~]$ sudo systemctl start httpd
    ```
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
    ```
    [dodo@web ~]$ sudo systemctl enable httpd
    ```
  - ouvrez le port firewall nécessaire
    ```
    [dodo@web ~]$ sudo firewall-cmd --add-port=80/tcp
    success
    [dodo@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success
    ```

🌞 **TEST**

- vérifier que le service est démarré et vérifier qu'il est configuré pour démarrer automatiquement
```
[dodo@web ~]$ systemctl status httpd
    ● httpd.service - The Apache HTTP Server
       Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled;     vendor preset: disabled)
       Active: active (running) since Wed 2021-09-29 17:31:07 CEST; 12min ago
         Docs: man:httpd.service(8)
     Main PID: 924 (httpd)
       Status: "Running, listening on: port 80"
        Tasks: 213 (limit: 11397)
       Memory: 29.3M
       CGroup: /system.slice/httpd.service
               ├─924 /usr/sbin/httpd -DFOREGROUND
               ├─947 /usr/sbin/httpd -DFOREGROUND
               ├─948 /usr/sbin/httpd -DFOREGROUND
               ├─949 /usr/sbin/httpd -DFOREGROUND
               └─950 /usr/sbin/httpd -DFOREGROUND

    Sep 29 17:31:07 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
    Sep 29 17:31:07 web.tp2.linux httpd[924]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using web.tp2.linux. Set the 'ServerName' directive globally to suppress this message
    Sep 29 17:31:07 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
    Sep 29 17:31:07 web.tp2.linux httpd[924]: Server configured, listening on: port 80
```
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
[dodo@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web
```
http://10.102.1.11:80/
```
Le serveur apache est bien accessible depuis l'hôte.

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume : 
```
[dodo@web ~]$ sudo systemctl enable httpd
```
- prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume : 
```
[dodo@web ~]$ sudo systemctl status httpd
```
rendu : 
```
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
...
```
- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```
[dodo@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf qui définit quel user est utilisé : 
```
[dodo@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
```
```
User apache
```
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf : 
```
apache       940     917  0 16:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       941     917  0 16:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       942     917  0 16:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       943     917  0 16:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
- vérifiez avec un `ls -al` le dossier du site (dans `/var/www/...`) 
  - vérifiez que tout son contenu appartient à l'utilisateur mentionné dans le fichier de conf : 
```
[dodo@web www]$ ls -al
total 4
drwxr-xr-x.  4 root root   33 Sep 29 17:24 .
drwxr-xr-x. 22 root root 4096 Sep 29 17:24 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
```

🌞 **Changer l'utilisateur utilisé par Apache**

```
[dodo@web ~]$ sudo useradd appache2 -m -s /sbin/nologin -u 1004
```
```
[dodo@web ~]$ sudo usermod -aG apache appache2
```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
[dodo@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
...
User appache2
Group apache
...
```
- redémarrez Apache
```
[dodo@web ~]$ sudo systemctl restart httpd
```
- utilisez une commande `ps` pour vérifier que le changement a pris effet
```
[dodo@web ~]$ ps -ef
...
appache2    2358    2356  0 17:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
appache2    2359    2356  0 17:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
appache2    2360    2356  0 17:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
appache2    2361    2356  0 17:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
...
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port : 
```
[dodo@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
Listen 8080
```
- ouvrez un nouveau port firewall, et fermez l'ancien : 
```
[dodo@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
```
```
[dodo@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
```
- redémarrez Apache : 
```
[dodo@web ~]$ sudo systemctl restart httpd
```
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi : 
```
[dodo@web ~]$ sudo ss -alnpt
State                         Recv-Q                        Send-Q                                               Local Address:Port                                               Peer Address:Port                       Process
LISTEN                        0                             128                                                        0.0.0.0:22                                                      0.0.0.0:*                           users:(("sshd",pid=918,fd=5))
LISTEN                        0                             128                                                           [::]:22                                                         [::]:*                           users:(("sshd",pid=918,fd=7))
LISTEN                        0                             128                                                              *:8080                                                          *:*                           users:(("httpd",pid=2664,fd=4),("httpd",pid=2663,fd=4),("httpd",pid=2662,fd=4),("httpd",pid=2659,fd=4))
```

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port : 
```
[dodo@web ~]$ sudo curl 10.102.1.11:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port : 
```
http://10.102.1.11:8080/
```
Le serveur apache fonctionne bien sur le nouveau port.

Le fichier [httpd.conf](tp2Linux/WEBhttpd.conf)

# II. Une stack web plus avancée

## 1. Intro

## 2. Setup

🖥️ **VM db.tp2.linux**

### A. Serveur Web et NextCloud

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`
```
[dodo@db ~]$ sudo dnf install epel-release
[dodo@db ~]$ sudo dnf update
[dodo@db ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[dodo@db ~]$ sudo dnf module list php
[dodo@db ~]$ sudo dnf module enable php:remi-7.4
[dodo@db ~]$ sudo dnf module list php
```
```
[dodo@db ~]$ sudo dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```
```
[dodo@db ~]$ sudo systemctl enable httpd
[dodo@db ~]$ vi /etc/httpd/sites-available/linux.tp2.web
[dodo@db ~]$ ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
[dodo@db ~]$ sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html
[dodo@db ~]$ timedatectl
[dodo@db ~]$ vi /etc/opt/remi/php74/php.ini
```
```
[dodo@db ~]$ ls -al /etc/localtime
lrwxrwxrwx. 1 root root 34 Sep 15 15:30 /etc/localtime -> ../usr/share/zoneinfo/Europe/Paris
```
```
[dodo@db ~]$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[dodo@db ~]$ sudo unzip nextcloud-21.0.1.zip
[dodo@db ~]$ sudo cd nextcloud
[dodo@db ~]$ sudo mv * /var/www/sub-domains/com.yourdomain.nextcloud/html/
[dodo@db ~]$ sudo chown -Rf apache.apache /var/www/sub-domains/com.yourdomain.nextcloud/html
[dodo@db ~]$ sudo mkdir /var/www/sub-domains/com.yourdomain.nextcloud/html/data
[dodo@db ~]$ sudo mv /var/www/sub-domains/com.yourdomain.nextcloud/html/data /var/www/sub-domains/com.yourdomain.nextcloud/
[dodo@db ~]$ sudo systemctl restart httpd
```
```
[dodo@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/httpd.service.d
           └─php74-php-fpm.conf
   Active: active (running) since Sat 2021-10-09 14:25:26 CEST; 1h 32min ago
     Docs: man:httpd.service(8)
 Main PID: 2271 (httpd)
   Status: "Total requests: 373; Idle/Busy workers 100/0;Requests/sec: 0.0669; Bytes served/sec: 7.2KB/sec"
    Tasks: 278 (limit: 11397)
   Memory: 36.8M
   CGroup: /system.slice/httpd.service
           ├─2271 /usr/sbin/httpd -DFOREGROUND
           ├─2272 /usr/sbin/httpd -DFOREGROUND
           ├─2273 /usr/sbin/httpd -DFOREGROUND
           ├─2274 /usr/sbin/httpd -DFOREGROUND
           ├─2275 /usr/sbin/httpd -DFOREGROUND
           └─2491 /usr/sbin/httpd -DFOREGROUND

Oct 09 14:25:26 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 09 14:25:26 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 09 14:25:26 web.tp2.linux httpd[2271]: Server configured, listening on: port 80
```

Le fichier [httpd.conf](tp2Linux/DBhttpd.conf) et le fichier [web.tp2.linux](tp2Linux/linux.tp2.web)

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```
[dodo@db ~]$ sudo dnf install mariadb-server
[dodo@db ~]$ sudo systemctl enable mariadb
[dodo@db ~]$ sudo systemctl start mariadb
[dodo@db ~]$ sudo mysql_secure_installation
[dodo@db ~]$ sudo ss
```
port 3306 pour la db donc : 
```
[dodo@db ~]$ sudo firewall-cmd --add-port=3306/tcp
[dodo@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[dodo@db ~]$ sudo firewall-cmd --reload
```
```
[dodo@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 **Préparation de la base pour NextCloud**

 ```
 sudo mysql -u root -p
 ```
Puis j'ai exécuté les commandes SQL suivantes :

```
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';

CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

FLUSH PRIVILEGES;
```


🌞 **Exploration de la base de données**

```
[dodo@db ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p
```

```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)
```
```
mysql> USE nextcloud
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
```
```
mysql> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| [...]	        	       |
| oc_whats_new                |
+-----------------------------+
99 rows in set (0.00 sec)
```

Pour lister les utilisateurs : 
```
SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.000 sec)
```

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

```
http://web.tp2.linux/index.php/apps/dashboard/
```
Après création de l'utilisateur admin, j'ai accès au dashboard de la DataBase sans soucis.

🌞 **Exploration de la base de données**

```
[dodo@web nextcloud]$ mysql -u nextcloud -h 10.102.1.12 -p
```

- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL



| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 22/tcp 80/tcp           | all             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 22/tcp  3306/tcp           | all             |

