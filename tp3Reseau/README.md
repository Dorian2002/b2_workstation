# B2_TP3_Réseau

# Sommaire

## I. (mini)Architecture réseau

### 1. Adressage

Voir dans le V. El final

### 2. Routeur

🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que : 

il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle : 
```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7f:26:cd brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.62/26 brd 10.3.1.63 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe7f:26cd/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:74:06:21 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.78/28 brd 10.3.1.79 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe74:621/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c7:04:bc brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.254/25 brd 10.3.1.255 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fec7:4bc/64 scope link
       valid_lft forever preferred_lft forever
```

il a un accès internet : 

```
[dodo@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=15.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=14.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=13.10 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=15.8 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 13.973/15.107/15.916/0.799 ms
```

il a de la résolution de noms : 

```
[dodo@router ~]$ ping google.com
PING google.com (142.250.201.46) 56(84) bytes of data.
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=1 ttl=112 time=26.5 ms
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=2 ttl=112 time=24.4 ms
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=3 ttl=112 time=26.10 ms
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=4 ttl=112 time=24.7 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 24.411/25.634/26.986/1.132 ms
```

il porte le nom router.tp3 : 

```
[dodo@router ~]$ hostname
router.tp3
```

n'oubliez pas d'activer le routage sur la machine : 

```
[dodo@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
[sudo] password for dodo:
success
[dodo@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

## II. Services d'infra

### 1. Serveur DHCP

🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra : 

porter le nom dhcp.client1.tp3 : 
```
[dodo@dhcp ~]$ hostname
dhcp.client1.tp3
```
donner une IP aux machines clients qui le demande, leur donner l'adresse de leur passerelle et leur donner l'adresse d'un DNS utilisable : 
```
[dodo@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.3.1.0 netmask 255.255.255.192 {
  range 10.3.1.1 10.3.1.35;
  option routers 10.3.1.62;
  option subnet-mask 255.255.255.192;
  option domain-name-servers 8.8.8.8;
}
```
[dhcpd.conf](tp3Reseau/dhcpd.conf)

🌞 Mettre en place un client dans le réseau client1 : 

de son p'tit nom marcel.client1.tp3 :
```
[dodo@marcel ~]$ hostname
marcel.client1.tp3
```
la machine récupérera une IP dynamiquement grâce au serveur DHCP ainsi que sa passerelle et une adresse d'un DNS utilisable : 
```
[dodo@marcel ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:09:e4:89 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.3/26 brd 10.3.1.63 scope global dynamic noprefixroute enp0s8
       valid_lft 793sec preferred_lft 793sec
    inet6 fe80::a00:27ff:fe09:e489/64 scope link
       valid_lft forever preferred_lft forever
```
```
[dodo@dhcp ~]$ sudo cat /var/lib/dhcpd/dhcpd.leases
...
lease 10.3.1.3 {
  starts 4 2021/09/30 14:53:38;
  ends 4 2021/09/30 15:08:38;
  cltt 4 2021/09/30 14:53:38;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:09:e4:89;
  uid "\001\010\000'\011\344\211";
  client-hostname "marcel";
}
```
```
[dodo@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for dodo:
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.3.1.0 netmask 255.255.255.192 {
  range 10.3.1.2 10.3.1.36;
  option routers 10.3.1.62;
  option subnet-mask 255.255.255.192;
  option domain-name-servers 8.8.8.8;
  option broadcast-address 10.3.1.63;
}
```

🌞 Depuis marcel.client1.tp3

prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP : 
```
[dodo@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=17.10 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=17.10 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=23.0 ms
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 17.983/19.661/23.012/2.372 ms
```
```
[dodo@marcel ~]$ ping google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=1 ttl=113 time=19.8 ms
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=2 ttl=113 time=19.4 ms
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 19.409/19.616/19.824/0.250 ms
```
À l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau : 
```
[dodo@marcel ~]$ traceroute google.com
traceroute to google.com (216.58.214.78), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.62)  4.185 ms  3.779 ms  3.696 ms
 2  10.0.2.2 (10.0.2.2)  3.472 ms  3.575 ms  3.174 ms
```

### 2. Serveur DNS

#### A. Our own DNS server

#### B. SETUP copain

🌞 Mettre en place une machine qui fera office de serveur DNS

dans le réseau server1 : 
```
[dodo@dns1 ~]$ ip a
...
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cc:b2:f1 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.253/25 brd 10.3.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fecc:b2f1/64 scope link
       valid_lft forever preferred_lft forever
```
de son p'tit nom dns1.server1.tp3 : 
```
[dodo@dns1 ~]$ hostname
dns1.server1.tp3
```
il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme google.com : 
```
[dodo@dns1 ~]$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=113 time=15.1 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=113 time=17.5 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=3 ttl=113 time=14.2 ms
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 14.192/16.235/18.160/1.636 ms
``` 

🌞 Tester le DNS depuis marcel.client1.tp3 : 

définissez manuellement l'utilisation de votre serveur DNS : 
```
[dodo@marcel ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search client1.tp3
nameserver 10.3.1.253
```

essayez une résolution de nom avec dig : 
```
[dodo@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8343
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 117801fc08041a994065ee716156c10fbed275c799ab4183 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       216.58.214.78

;; AUTHORITY SECTION:
google.com.             172800  IN      NS      ns1.google.com.
google.com.             172800  IN      NS      ns3.google.com.
google.com.             172800  IN      NS      ns4.google.com.
google.com.             172800  IN      NS      ns2.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172800  IN      A       216.239.34.10
ns1.google.com.         172800  IN      A       216.239.32.10
ns3.google.com.         172800  IN      A       216.239.36.10
ns4.google.com.         172800  IN      A       216.239.38.10
ns2.google.com.         172800  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172800  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172800  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172800  IN      AAAA    2001:4860:4802:38::a

;; Query time: 281 msec
;; SERVER: 10.3.1.253#53(10.3.1.253)
;; WHEN: Fri Oct 01 10:04:31 CEST 2021
;; MSG SIZE  rcvd: 331
```
prouvez que c'est bien votre serveur DNS qui répond pour chaque dig : 
```
[dodo@marcel ~]$ dig dns1.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49896
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 76a134ff48a2869f98eaf2356156c16355e00952716382fc (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.1.253

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 0 msec
;; SERVER: 10.3.1.253#53(10.3.1.253)
;; WHEN: Fri Oct 01 10:05:55 CEST 2021
;; MSG SIZE  rcvd: 103
```

🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds : 

```
[dodo@dns1 ~]$ sudo cat /etc/named.conf
...
zone "server1.tp3." IN {
        type master;
        file "server1.tp3.forward";
        allow-update { none; };
};

zone "client1.tp3." IN {
        type master;
        file "client1.tp3.forward";
        allow-update { none; };
};
...
```
```
[dodo@dns1 ~]$ sudo ls /var/named
client1.tp3.forward  dynamic   named.empty      named.loopback      server1.tp3.forward
data                 named.ca  named.localhost  slaves
```

```
[dodo@marcel ~]$ ping dns1.server1.tp3
PING dns1.server1.tp3 (10.3.1.253) 56(84) bytes of data.
64 bytes from 10.3.1.253 (10.3.1.253): icmp_seq=1 ttl=63 time=0.634 ms
64 bytes from 10.3.1.253 (10.3.1.253): icmp_seq=2 ttl=63 time=1.84 ms
--- dns1.server1.tp3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.634/1.234/1.835/0.601 ms
```
```
[dodo@router ~]$ ping dns1.server1.tp3
PING dns1.server1.tp3 (10.3.1.253) 56(84) bytes of data.
64 bytes from 10.3.1.253 (10.3.1.253): icmp_seq=1 ttl=64 time=0.486 ms
64 bytes from 10.3.1.253 (10.3.1.253): icmp_seq=2 ttl=64 time=1.13 ms
--- dns1.server1.tp3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.486/0.808/1.131/0.323 ms
```
```
[dodo@dhcp ~]$ ping dns1.server1.tp3
PING dns1.server1.tp3 (10.3.1.253) 56(84) bytes of data.
64 bytes from 10.3.1.253 (10.3.1.253): icmp_seq=1 ttl=63 time=0.952 ms
64 bytes from 10.3.1.253 (10.3.1.253): icmp_seq=2 ttl=63 time=0.774 ms
--- dns1.server1.tp3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.774/0.863/0.952/0.089 ms
```

### 3. Get deeper

#### A. DNS forwarder

🌞 Affiner la configuration du DNS : 
```
[dodo@dns1 ~]$ sudo cat /etc/named.conf
[...]
recursion yes;
[...]
```
🌞 Test ! : 
```
[dodo@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 15956
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 8794f11f462903def705c908615c102288d9b93980e1f636 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             207     IN      A       216.58.213.78

;; AUTHORITY SECTION:
google.com.             172707  IN      NS      ns3.google.com.
google.com.             172707  IN      NS      ns1.google.com.
google.com.             172707  IN      NS      ns4.google.com.
google.com.             172707  IN      NS      ns2.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172707  IN      A       216.239.34.10
ns1.google.com.         172707  IN      A       216.239.32.10
ns3.google.com.         172707  IN      A       216.239.36.10
ns4.google.com.         172707  IN      A       216.239.38.10
ns2.google.com.         172707  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172707  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172707  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172707  IN      AAAA    2001:4860:4802:38::a

;; Query time: 0 msec
;; SERVER: 10.3.1.253#53(10.3.1.253)
;; WHEN: Tue Oct 05 10:43:15 CEST 2021
;; MSG SIZE  rcvd: 331
```

#### B. On revient sur la conf du DHCP : 

🌞 Affiner la configuration du DHCP : 
dhcpd.conf : 
```
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.3.1.0 netmask 255.255.255.192 {
  range 10.3.1.2 10.3.1.36;
  option routers 10.3.1.62;
  option subnet-mask 255.255.255.192;
  option domain-name-servers 10.3.1.253;
  option broadcast-address 10.3.1.63;
}
```

```
[dodo@johnny ~]$ ip a
...
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f3:46:67 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.4/26 brd 10.3.1.63 scope global dynamic noprefixroute enp0s8
       valid_lft 859sec preferred_lft 859sec
    inet6 fe80::a00:27ff:fef3:4667/64 scope link
       valid_lft forever preferred_lft forever
```
```
[dodo@johnny ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 40029
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 5f3b47ee4757196d6af3d2ac615c12e4c95d861445f6fef8 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       216.58.213.78

;; AUTHORITY SECTION:
google.com.             172001  IN      NS      ns2.google.com.
google.com.             172001  IN      NS      ns3.google.com.
google.com.             172001  IN      NS      ns4.google.com.
google.com.             172001  IN      NS      ns1.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172001  IN      A       216.239.34.10
ns1.google.com.         172001  IN      A       216.239.32.10
ns3.google.com.         172001  IN      A       216.239.36.10
ns4.google.com.         172001  IN      A       216.239.38.10
ns2.google.com.         172001  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172001  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172001  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172001  IN      AAAA    2001:4860:4802:38::a

;; Query time: 22 msec
;; SERVER: 10.3.1.253#53(10.3.1.253)
;; WHEN: Tue Oct 05 10:55:00 CEST 2021
;; MSG SIZE  rcvd: 331
```

## III. Services métier

### 1. Serveur Web

🌞 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients : 

réseau server2 : 
```
[dodo@localhost ~]$ ip a
...
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:36:b9:0d brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.77/28 brd 10.3.1.79 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe36:b90d/64 scope link
       valid_lft forever preferred_lft forever
```

hello web1.server2.tp3 : 
```
[dodo@localhost ~]$ hostname
web1.server2.tp3
```

vous utiliserez le serveur web que vous voudrez, le but c'est d'avoir un serveur web fast, pas d'y passer 1000 ans : 
```
[dodo@localhost ~]$ systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-05 16:54:42 CEST; 24min ago
 Main PID: 20847 (python3)
    Tasks: 1 (limit: 4946)
   Memory: 9.7M
   CGroup: /system.slice/web.service
           └─20847 /bin/python3 -m http.server 8888

Oct 05 16:54:42 web1.server2.tp3 systemd[1]: Started Very simple web service.
```

🌞 Test test test et re-test : 
```
[dodo@marcel ~]$ curl web1.server2.tp3:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

### 2. Partage de fichiers

#### A. L'introduction wola

#### B. Le setup wola

🌞 Setup d'une nouvelle machine, qui sera un serveur NFS : 

**réseau server2 :** 

```
[dodo@nfs1 ~]$ ip a
...
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7b:c7:e4 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.76/25 brd 10.3.1.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe7b:c7e4/64 scope link
       valid_lft forever preferred_lft forever
```
**son nooooom : nfs1.server2.tp3 :**
```
[dodo@nfs1 ~]$ hostname
nfs1.server2.tp3
```
**nfs server rocky linux :** 

modification de /etc/idmapd.conf : 
```
[dodo@nfs1 ~]$ sudo cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = server2.tp3
[...]
```
modification de /etc/exports
```
[dodo@nfs1 ~]$ sudo cat /etc/exports
/srv/nfs_share/ 10.3.1.77/28(rw,no_root_squash)
```
dossier nfs_share : 
```
[dodo@nfs1 srv]$ ls -all
total 0
drwxr-xr-x.  3 root root  23 Oct 14 10:46 .
dr-xr-xr-x. 17 root root 224 Sep 15 15:25 ..
drwxr-xr-x.  2 root root   6 Oct 14 10:46 nfs_share
```
fonctionnement du serveur : 
```
[dodo@nfs1 srv]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
  Drop-In: /run/systemd/generator/nfs-server.service.d
           └─order-with-mounts.conf
   Active: active (exited) since Thu 2021-10-14 10:40:47 CEST; 21min ago
  Process: 1594 ExecReload=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 1257 (code=exited, status=0/SUCCESS)
```
🌞 Configuration du client NFS : 

dossier nfs : 
```
[dodo@web1 srv]$ ls -al
total 0
drwxr-xr-x.  3 root root  17 Oct 14 10:49 .
dr-xr-xr-x. 19 root root 247 Oct  8 16:34 ..
drwxr-xr-x.  2 root root   6 Oct 14 10:46 nfs
```
install nfs :
```
dnf -y install nfs-utils
```
modification /etc/idmapd.conf : 
```
[dodo@web1 srv]$ sudo cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = server2.tp3
[...]
```
Le montage : 
```
[dodo@web1 nfs]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
```
La partition : 
```
[dodo@web1 /]$ df -hT
[...]
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.1G  4.2G  34% /srv/nfs
```
🌞 TEEEEST
côté web1 : 
```
[dodo@web1 nfs]$ sudo touch test
```
côté nfs1 : 
```
[dodo@nfs1 nfs_share]$ ls -al
total 0
drwxr-xr-x. 2 root root 18 Oct 14 11:37 .
drwxr-xr-x. 3 root root 23 Oct 14 10:46 ..
-rw-r--r--. 1 root root  0 Oct 14 11:37 test
[dodo@nfs1 nfs_share]$ sudo vi test
```
re côté web1 : 
```
[dodo@web1 nfs]$ sudo cat test
yo
```

## IV. Un peu de théorie : TCP et UDP

🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP : 
ssh : 
```
Transmission Control Protocol, Src Port: 22, Dst Port: 10114, Seq: 1, Ack: 1, Len: 148
```
http : 
```
Transmission Control Protocol, Src Port: 49394, Dst Port: 8888, Seq: 1, Ack: 1, Len: 78
```
dns : 
```
User Datagram Protocol, Src Port: 36290, Dst Port: 53
```
nfs : 
```
Transmission Control Protocol, Src Port: 912, Dst Port: 2049, Seq: 1, Ack: 
```

🌞 Capturez et mettez en évidence un 3-way handshake : 
```
1	0.000000	10.3.1.78	10.3.1.77	TCP	74	49394 → 8888 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM=1 TSval=3890363863 TSecr=0 WS=128
2	0.000056	10.3.1.77	10.3.1.78	TCP	74	8888 → 49394 [SYN, ACK] Seq=0 Ack=1 Win=28960 Len=0 MSS=1460 SACK_PERM=1 TSval=498969869 TSecr=3890363863 WS=128
3	0.000974	10.3.1.78	10.3.1.77	TCP	66	49394 → 8888 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=3890363864 TSecr=498969869
```

## V. El final

🌞 Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme :-1: 

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | Adresse broadcast|
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.1.0`        | `255.255.255.192` |`62`                            | `10.3.1.63`         | `10.3.1.64`                                                                                   |
| `server1`     | `10.3.1.128`        | `255.255.255.128` | `126`                           | `10.3.1.254`         | `10.3.1.255`                                                                                   |
| `server2`     | `10.3.1.65`        | `255.255.255.240` | `14`                           | `10.3.1.79`         | `10.3.1.80`

🌞 Vous remplirez aussi au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, le 🗃️ tableau d'adressage 🗃️ suivant :

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.1.62/26`         | `10.3.1.254/25`         | `10.3.1.78/28`         | Carte NAT             |
| `marcel.client1.tp3`          | `10.3.1.2`                | /                  | /                  | `10.3.1.62/26`          |
| `johnny.client1.tp3`          | `10.3.1.4`                | /                  | /                  | `10.3.1.62/26`          |
| `dhcp.client1.tp3`          | `10.3.1.61`                | /                  | /                  | `10.3.1.62/26`          |
| `dns1.server1.tp3`          | /                | `10.3.1.253/25`                  | /                  | `10.3.1.254/25`          |
| `nfs1.server2.tp3`          | /                | /                  | `10.3.1.76/28`                  | `10.3.1.78/28`          |
| `web1.server2.tp3`          | /                | /                  | `10.3.1.77/28`                  | `10.3.1.78/28`          |


🌞 Bah j'veux un schéma.

![](tp3Reseau/Diagram.png)
	
🌞 Et j'veux des fichiers aussi, tous les fichiers de conf du DNS

[named.conf](tp3Reseau/named.conf)
[client1.tp3.forward](tp3Reseau/client1.tp3.forward)
[server1.tp3.forward](tp3Reseau/server1.tp3.forward)
[server2.tp3.forward](tp3Reseau/server2.tp3.forward)
