# B2_TP1_Linux

## 0. Préparation de la machine

#### 🌞 Setup de deux machines Rocky Linux configurée de façon basique.

Un accès internet (via la carte NAT) : 
*    ```ip a```  : 
```
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3d:c4:a0 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85470sec preferred_lft 85470sec
    inet6 fe80::a00:27ff:fe3d:c4a0/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

* route par défaut : 
```
[dodo@node2 ~]$ [dodo@node2 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.12 metric 101
```

Un accès à un réseau local (les deux machines peuvent se ping) (via la carte Host-Only) : 
* les machines doivent posséder une IP statique sur l'interface host-only :
    ```
    TYPE=Ethernet
    PROXY_METHOD=none
    BROWSER_ONLY=no
    BOOTPROTO=static
    DEFROUTE=yes
    IPV4_FAILURE_FATAL=no
    IPV6INIT=yes
    IPV6_AUTOCONF=yes
    IPV6_DEFROUTE=yes
    IPV6_FAILURE_FATAL=no
    NAME=enp0s8
    UUID=4dfd674c-aca9-4074-8b06-f0943aceab31
    DEVICE=enp0s8
    ONBOOT=on
    IPADDR=10.101.1.12
    NETMASK=255.255.255.0
    ```

    Elles peuvent se ping : 
    ```
    [dodo@node2 ~]$ ping 10.101.1.11
    PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
    64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.582 ms
    64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.477 ms
    64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=1.14 ms
    64 bytes from 10.101.1.11: icmp_seq=4 ttl=64 time=1.02 ms
    ^C
    --- 10.101.1.11 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3053ms
    rtt min/avg/max/mdev = 0.477/0.803/1.138/0.282 ms 
    ```
    
Les machines doivent avoir un nom :
    ```
    [dodo@node2 ~]$ hostname
node2.tp1.b2
    ```

utiliser 1.1.1.1 comme serveur DNS : 
```
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s3
UUID=7da91c7f-cc82-469f-aff9-99ed8a129376
DEVICE=enp0s3
ONBOOT=yes
DNS=1.1.1.1
```
Résultat avec dig :
```
ynov.com.               205     IN      A       92.243.16.143
```
```
;; SERVER: 10.33.10.2#53(10.33.10.2)
```

les machines doivent pouvoir se joindre par leurs noms respectifs : 

```sudo nano /etc/hosts``` :
```10.101.1.12 node2.tp1.b2```

```
[dodo@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.342 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.211 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.604 ms
^C
--- node2.tp1.b2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2042ms
rtt min/avg/max/mdev = 0.211/0.385/0.604/0.164 ms
```

Pareil pour la node2.tp1.b2

le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires : 

```
[dodo@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que : 
```
[dodo@node1 ~]$ sudo useradd toto -m -s /bin/bash
```
sudo cat /etc/passwd :
```
toto:x:1001:1001::/home/toto:/bin/bash
```

🌞 Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.

dans le fichier ```/etc/sudoers```: 
```
%admins ALL=(ALL)       ALL
```
Puis création du groupe : 
```
sudo groupadd admins
```

🌞 Ajouter votre utilisateur à ce groupe admins.
Ajout de l'utilisateur au groupe : 
```
sudo usermod -aG admins toto
```

### 2. SSH

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine.
🌞 Pour cela : 
Le fichier authized_keys :
```
[toto@node1 .ssh]$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyvVbihNH656Laz8eWVbIJxS8JkuRyJs5k58lhr7Ua9lAO51ZaFfv/mx4DkctP8h8BQXgrzSqtIM20tJ7D8WxNc1zy48zu8idb4Vwuj7pJx039NhpqPySqC1nWqw4vLh+m0WWYtsrewQk7Om3fPoZt00bYogBdqeTzLBKnjgY1fxAk1Hyr5adc8C0xV/djVTInv0CyGKeRs67mWztWucNctAkN/3Agnqq1b41UdicygjnrpcvgVQHEcvPvyPPpZNbabmtWfIAlyG3u1wEjxgN4ZfPeVy54sM7drHbysvW+rWAjBNc2neOKg1I0w28rjmnV7pw/HJox7P8d1XYm9jiKc4gX2Gxkd2AXspFkzAMeh5Wuo5/EhdqrMMpxKv2liVgA5qck6naq//JOlXRAkkZJra5Zpg98I1QhYsZre25ONaceW99KjoaQRTvbxMTcB3sMLMF5aE1sIQX0roB8QD0gqqHLbhz24Xgoro/ZLrfSlrN5a8hUOXTCf1293jCmPCeaPgCniAxekP51n3v1/akAB/W8NzRY4F4BCT1mfGTIAyVrAY95ll0tklZudlltSSy0P2wJ4ReLrrNxcPq66LDtmZmfRg98IMQb0u1PJfkfl10eLZ9fpDxZPXb0l1jmnr/cNhszF/mpF8p4N5qu3dZXp7X0LzjI/R83+kGt+cDDHQ== User@LAPTOP-HBUG3T8J
```
La connexion sans mot de passe : 
```
PS C:\Users\User> ssh toto@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Sep 23 13:40:50 2021 from 10.101.1.1
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Sep 23 13:40:50 2021 from 10.101.1.1
```

 ## II. Partitionnement

### 1. Préparation de la VM
```
"/dev/sdb" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               XJbb8Z-PVl4-AJ8O-Cb79-M8L4-EXTw-Wd5NyG

  "/dev/sdc" is a new physical volume of "3.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdc
  VG Name
  PV Size               3.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               zljGL3-MuPa-ytIW-rJMJ-2YmV-b9Kf-DVnFGD
```
### 2. Partitionnement
🌞 Utilisez LVM pour : 

agréger les deux disques en un seul volume group : 
```
[dodo@node1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sdb
  VG Name               D1
  PV Size               3.00 GiB / not usable 4.00 MiB
  Allocatable           yes
  PE Size               4.00 MiB
  Total PE              767
  Free PE               767
  Allocated PE          0
  PV UUID               XJbb8Z-PVl4-AJ8O-Cb79-M8L4-EXTw-Wd5NyG

  --- Physical volume ---
  PV Name               /dev/sdc
  VG Name               D1
  PV Size               3.00 GiB / not usable 4.00 MiB
  Allocatable           yes
  PE Size               4.00 MiB
  Total PE              767
  Free PE               767
  Allocated PE          0
  PV UUID               wGwvtA-utsx-t8JY-JRgy-AeG0-cJeV-BlEpxE
```

créer 3 logical volumes de 1 Go chacun : 

```
[dodo@node1 ~]$ sudo lvs
  LV   VG Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  D1_1 D1 -wi-a-----   1.00g
  D1_2 D1 -wi-a-----   1.00g
  D1_3 D1 -wi-a-----   1.00g
  root rl -wi-ao----  <6.20g
  swap rl -wi-ao---- 820.00m
```

formater ces partitions en ext4 : 
 ```
 [dodo@node1 ~]$ sudo mkfs -t ext4 /dev/mapper/D1-D1_1
 [dodo@node1 ~]$ sudo mkfs -t ext4 /dev/mapper/D1-D1_2
 [dodo@node1 ~]$ sudo mkfs -t ext4 /dev/mapper/D1-D1_3
 ```

monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3 : 
```
[dodo@node1 ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             890M     0  890M   0% /dev
tmpfs                909M     0  909M   0% /dev/shm
tmpfs                909M  8.6M  901M   1% /run
tmpfs                909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root  6.2G  1.9G  4.4G  30% /
/dev/sda1           1014M  182M  833M  18% /boot
tmpfs                182M     0  182M   0% /run/user/1000
/dev/mapper/D1-D1_1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/D1-D1_2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/D1-D1_3  976M  2.6M  907M   1% /mnt/part3
```

🌞 Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

Suite à un reboot les partitions sont toujours là : 
```
/dev/mapper/D1-D1_3  976M  2.6M  907M   1% /mnt/part3
/dev/mapper/D1-D1_1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/D1-D1_2  976M  2.6M  907M   1% /mnt/part2
```

## III. Gestion de services

### 1. Interaction avec un service existant

🌞 Assurez-vous que (firewalld):

l'unité est démarrée : 
```
[dodo@node1 ~]$ sudo systemctl status firewalld
...
   Active: active (running) since Sun 2021-09-26 12:29:50 CEST; 4min 23s ago
...
```

l'unitée est activée (elle se lance automatiquement au démarrage)

```
[dodo@node1 ~]$ sudo systemctl status firewalld
...
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
...
```
Le service firewalld est "enable" il se lance donc automatiquement à chaque boot.

### 2. Création de service

#### A. Unité simpliste

🌞 Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system : 

```
[dodo@node1 system]$ ls -all
...
-rw-r--r--.  1 root root  136 Sep 26 12:42 web.service
```

🌞Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888 : 

```
[dodo@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href=".ssh/">.ssh/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

#### B. Modification de l'unité

🌞 Créer un utilisateur web.

```
[dodo@node1 ~]$ sudo useradd web
```

🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses : 

```
[dodo@node1 system]$ sudo cat web.service
[sudo] password for dodo:
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=WebDir

[Install]
WantedBy=multi-user.target
```

🌞 Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web : 

```
[dodo@node1 WebDir]$ ls -all
...
-rw-r--r--. 1 web  web   5 Sep 26 13:19 test
```

🌞 Vérifier le bon fonctionnement avec une commande curl : 

```
[web@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href=".ssh/">.ssh/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```