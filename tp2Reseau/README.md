# B2_TP2_Réseau

## I. ARP

### 1. Echange ARP

🌞Générer des requêtes ARP

Ping entre les VMs
```
[dodo@bastion-ovh1fr ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.856 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.573 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.960 ms
```
Table ARP des deux VMs : 
node 1
```
[dodo@bastion-ovh1fr ~]$ ip n s
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
10.2.1.12 dev enp0s8 lladdr 08:00:27:ba:1a:e2 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:52 DELAY
```
node 2
```
[dodo@bastion-ovh1fr ~]$ ip n s
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:f6:95:d5 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:52 REACHABLE
```
Commande ```ip a``` sur node2 : 
```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ba:1a:e2 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feba:1ae2/64 scope link
       valid_lft forever preferred_lft forever
```
On peut voir que les adresses mac

### 2. Analyse de trames

🌞Analyse de trames

Je vide les tables ARP avec ```sudo ip neigh flush all```.
Je lance la capture avec ```sudo tcpdump -i enp0s8 -c 10 -w tp2_arp.pcap not port 22```

Je rentre le fichier sur Wireshark : 
![](https://i.imgur.com/oL0Pgjo.png)

| ordre | type trame  | source                      | destination                |
|-------|-------------|-----------------------------|----------------------------|
| 1     | Requête ARP | `node1` `00:27:f6:95:d5` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `00:27:ba:1a:e2` | `node1` `00:27:f6:95:d5`   

## II. Routage

### 1. Mise en place du routage

🌞Activer le routage sur le noeud router.net2.tp2

```
[dodo@marcel ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
[sudo] password for dodo:
success
```

🌞Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping
```
[dodo@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
```
```
[dodo@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=144 ttl=63 time=1.15 ms
64 bytes from 10.2.2.12: icmp_seq=145 ttl=63 time=1.03 ms
64 bytes from 10.2.2.12: icmp_seq=146 ttl=63 time=1.10 ms
64 bytes from 10.2.2.12: icmp_seq=147 ttl=63 time=0.708 ms
```
### 2. Analyse de trames

🌞Analyse des échanges ARP

Vidage de la table ARP : 
```
sudo ip neigh flush all
```

Mise en écoute : 
```
[dodo@node1 ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_routage_node1.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `00:27:f6:95:d5`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `router` `00:27:51:ca:0b` | x              | `node1` `00:27:f6:95:d5`   |
| 3     | Requête ARP | x         | `node1` `00:27:f6:95:d5`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 4     | Réponse ARP | x         | `router` `00:27:51:ca:0b` | x              | `node1` `00:27:f6:95:d5`   |
| 5     | Requête ARP | x         | `router` `00:27:60:63:35`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 6     | Réponse ARP | x         | `marcel` `00:27:b3:6d:63` | x              | `router` `00:27:60:63:35`   |
| 7     | Ping        | 10.2.1.11         | `node1` `00:27:f6:95:d5`                         | 10.2.2.12              | `marcel` `00:27:b3:6d:63`                          |
| 8     | Pong        | 10.2.2.12              | `marcel` `00:27:b3:6d:63`|                10.2.1.11         | `node1` `00:27:f6:95:d5`

### 3. Accès internet

🌞Donnez un accès internet à vos machines

Ajout de la route par défaut : 
```
sudo ip route add default via 10.2.1.254 dev enp0s8
```
ping de vérification : 
```
[dodo@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=16.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=15.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=16.8 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=14.9 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 14.895/16.041/16.798/0.779 ms
```

Ajout d'un DNS : 
```
[dodo@node1 ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
```

🌞Analyse de trames

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |     |
|-------|------------|---------------------|--------------------------|----------------|-----------------|-----|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `00:27:f6:95:d5` | `8.8.8.8`      | `00:27:51:ca:0b`               |
| 2     | pong       | `8.8.8.8`                 | `00:27:51:ca:0b`                      | `node1` `10.2.1.12`            | `node1` `00:27:f6:95:d5`             |

## III. DHCP

### 1. Mise en place du serveur DHCP

🌞Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server")

Après avoir config le fichier ```dhcpd.conf``` je lance le service dhcp : 
```
[dodo@node1 ~]$ sudo systemctl enable --now dhcpd
```
node 2 c'est bien configuré sur le réseau avec le serveur dhcp de node 1.
```
[dodo@node2 ~]$ ip a
...
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ba:1a:e2 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 661sec preferred_lft 661sec
    inet6 fe80::a00:27ff:feba:1ae2/64 scope link
       valid_lft forever preferred_lft forever
[dodo@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=no
```

🌞Améliorer la configuration du DHCP

Configuration du server dhcp node1 : 
```
[dodo@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for dodo:
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.12 10.2.1.112;
  option routers 10.2.1.254;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 10.2.1.1;
  option domain-name-servers 8.8.8.8;
}
```
Configuration des cartes réseaux node2 : 
```
[dodo@node2 ~]$ [dodo@node2 ~]$ ip a
...
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:19:35:b3 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ba:1a:e2 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 499sec preferred_lft 499sec
    inet6 fe80::a00:27ff:feba:1ae2/64 scope link
       valid_lft forever preferred_lft forever
```
Ping passerelle : 
```
[dodo@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.405 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.902 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=0.366 ms
^C
--- 10.2.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2027ms
rtt min/avg/max/mdev = 0.366/0.557/0.902/0.245 ms
```
Route par défaut : 
```
[dodo@node2 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 101
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 101
```

Ping vers Marcel : 
```
[dodo@node2 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.28 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.847 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=0.379 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2050ms
rtt min/avg/max/mdev = 0.379/0.835/1.279/0.367 ms
```

Vérification du DNS : 
```
[dodo@node2 ~]$ dig
;; SERVER: 8.8.8.8#53(8.8.8.8)
```

Ping vers ovh.com : 
```
[dodo@node2 ~]$ ping ovh.com
PING ovh.com (198.27.92.1) 56(84) bytes of data.
64 bytes from www.ovh.com (198.27.92.1): icmp_seq=1 ttl=48 time=18.2 ms
64 bytes from www.ovh.com (198.27.92.1): icmp_seq=2 ttl=48 time=19.7 ms
64 bytes from www.ovh.com (198.27.92.1): icmp_seq=3 ttl=48 time=24.0 ms
^C
--- ovh.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 18.218/20.651/24.018/2.463 ms
```
### 2. Analyse de trames

🌞Analyse de trames

Echange DHCP : 
![](https://i.imgur.com/CYOkLfm.png)

| ordre | type trame  | source                      | destination                |
|-------|-------------|-----------------------------|----------------------------|
| 1     | ARP | `node1` `00:27:f6:95:d5` | `node2` `00:27:ba:1a:e2` |
| 2     | ARP | `node2` `00:27:ba:1a:e2` | `node1` `00:27:f6:95:d5`   
| 3     | DHCP Discover | `node2` `00:27:ba:1a:e2` | Broadcast `FF:FF:FF:FF:FF` |
| 4     | DHCP Offer | `node1` `00:27:f6:95:d5` | `node2` `00:27:ba:1a:e2`   
| 5     | DHCP Request | `node2` `00:27:ba:1a:e2` | Broadcast `FF:FF:FF:FF:FF` |
| 6     | DHCP ACK | `node1` `00:27:f6:95:d5` | `node2` `00:27:ba:1a:e2`   
