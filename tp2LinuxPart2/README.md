# B2_TP2_Linux_Part2

# Sommaire

- [TP2 pt. 2 : Maintien en condition opérationnelle](#tp2-pt-2--maintien-en-condition-opérationnelle)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Monitoring](#i-monitoring)
  - [1. Le concept](#1-le-concept)
  - [2. Setup](#2-setup)
- [II. Backup](#ii-backup)
  - [1. Intwo bwo](#1-intwo-bwo)
  - [2. Partage NFS](#2-partage-nfs)
  - [3. Backup de fichiers](#3-backup-de-fichiers)
  - [4. Unité de service](#4-unité-de-service)
    - [A. Unité de service](#a-unité-de-service)
    - [B. Timer](#b-timer)
    - [C. Contexte](#c-contexte)
  - [5. Backup de base de données](#5-backup-de-base-de-données)
  - [6. Petit point sur la backup](#6-petit-point-sur-la-backup)
- [III. Reverse Proxy](#iii-reverse-proxy)
  - [1. Introooooo](#1-introooooo)
  - [2. Setup simple](#2-setup-simple)
  - [3. Bonus HTTPS](#3-bonus-https)
- [IV. Firewalling](#iv-firewalling)
  - [1. Présentation de la syntaxe](#1-présentation-de-la-syntaxe)
  - [2. Mise en place](#2-mise-en-place)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web](#b-serveur-web)
    - [C. Serveur de backup](#c-serveur-de-backup)
    - [D. Reverse Proxy](#d-reverse-proxy)
    - [E. Tableau récap](#e-tableau-récap)

# 0. Prérequis

➜ [TP2 Part.1](../part1/README.md) terminé : on doit avoir un NextCloud et sa base de données dédiée

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.102.1.0/24`.**

➜ Chaque **création de machines** sera indiqué par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel avec un échange de clé
- [x] accès Internet (une route par défaut, une carte NAT c'est très bien)
- [x] résolution de nom
  - résolution de noms publics, en ajoutant un DNS public à la machine
  - résolution des noms du TP, à l'aide du fichier `/etc/hosts`
- [ ] monitoring (oui, toutes les machines devront être surveillées)

# I. Monitoring

On bouge pas pour le moment niveau machines :

| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?             |

## 1. Le concept

La surveillance ou *monitoring* consiste à surveiller la bonne santé d'une entité.  
J'utilise volontairement le terme vague "entité" car cela peut être très divers :

- une machine
- une application
- un lien entre deux machines
- etc.

---

**Le monitoring s'effectue en plusieurs étapes :**

- ***scraping***
  - un programme tourne sur la machine pour récupérer des métriques sur le système
  - récupérer l'état de remplissage de la RAM par exemple
- ***centralisation*** des données (optionnel)
  - dans le cas d'un gros parc de machines, les métriques récupérées sont centralisées sur un unique serveur
- ***visualisation*** des données
  - une joulie interface est dispo pour visualiser les métriques
  - des courbes dans tous les sens
- ***alerting***
  - l'administrateur définit des seuils critiques pour certaines métriques
  - si le seuil est dépassé, une alerte est envoyée (mail, Discord, Slack, etc.) pour être prévenu immédiatement d'un soucis
  - dans le cas de la RAM par exemple, on sera prévenus **avant** que la RAM soit remplie

---

**Dans notre cas on va surveiller deux choses :**

- d'une part, les machines : ***monitoring système***. Par exemple :
  - remplissage disque/RAM
  - charge CPU/réseau
- d'autre part, nos applications : ***monitoring applicatif***. Ici :
  - serveur Web
  - base de données

## 2. Setup

De nombreuses solutions de monitoring existent sur le marché. Nous, on va utiliser [Netdata](https://www.netdata.cloud/).  

Maintenant, vous êtes des techs. Alors la page qui vous intéresse encore plus, c'est [le dépôt git de la solution](https://github.com/netdata/netdata).

- le README.md y est souvent très complet
- présente la solution, et les étapes d'install
- fournit les liens vers la doc

🌞 **Setup Netdata**

- y'a plein de méthodes d'install pour Netdata
- on va aller au plus simple, exécutez, sur toutes les machines que vous souhaitez monitorer :

```bash
$ sudo su -
$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
$ exit
```
```
[...]
[/tmp/netdata-kickstart-P0cV0jTnWG]# rm -rf /tmp/netdata-kickstart-P0cV0jTnWG
 OK
[...]
```

🌞 **Manipulation du *service* Netdata**

```
[dodo@web ~]$ sudo systemctl status netdata
[sudo] password for dodo:
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:40:29 CEST; 16min ago
[...]
```
```
LISTEN            0                 128                                0.0.0.0:19999                            0.0.0.0:*               users:(("netdata",pid=2374,fd=5))
```
```
[dodo@web ~]$ sudo firewall-cmd --add-port=19999/tcp
[sudo] password for dodo:
success
[dodo@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[dodo@web ~]$ sudo firewall-cmd --reload
success
[dodo@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp 80/tcp 19999/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

**Eeeeet.... c'est tout !**, rendez-vous sur `http://IP_VM:PORT` pour accéder à l'interface Web de Netdata (depuis un navigateur sur votre PC).  
**C'est sexy na ? Et c'est en temps réel :3**

🌞 **Setup Alerting**
```
/opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
```
Contenu du fichier : 
```
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897134121833365585/3RKFHbCJRWqlZbER-Ax2NdGTcauO_Eu4DZA1RvmmYw_v2xFgKwYOJO--vJk5S6MGTI6G"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```
config de la notif : 
```
sudo passwd netdata
sudo su -s /bin/bash netdata
export NETDATA_ALARM_NOTIFY_DEBUG=1
/opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
```
Notification discord : 
```
netdata on db.tp2.linux
BOT
 — Aujourd’hui à 14:11
db.tp2.linux needs attention, test.chart (test.family), test alarm = new value
test alarm = new value
this is a test alarm to verify notifications work
test.chart
test.family


db.tp2.linux•Aujourd’hui à 14:11
[14:11]
db.tp2.linux is critical, test.chart (test.family), test alarm = new value
test alarm = new value
this is a test alarm to verify notifications work
test.chart
test.family


db.tp2.linux•Aujourd’hui à 14:11
[14:11]
db.tp2.linux recovered, test.chart (test.family), test alarm (alarm was raised for 3 seconds)
test alarm (alarm was raised for 3 seconds)
this is a test alarm to verify notifications work
test.chart
test.family


db.tp2.linux•Aujourd’hui à 14:11
```

🌞 **Config alerting**
```
[dodo@web ~]$ sudo /opt/netdata/etc/netdata/edit-config health.d/ram.conf
```
dans le fichiers : 
```
 alarm: ram_usage
 on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
 warn: $this > 50
 crit: $this > 50
 info: The percentage of RAM used by the system.
```
restart netdata : 
```
[dodo@db ~]$ sudo systemctl restart netdata
```
La petite commande magique pour que les notifs fonctionnent : 
```
sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```
Install de stress : 
```
[dodo@db ~]$ sudo dnf -y install epel-release
[dodo@web ~]$ sudo dnf install stress
```
On bourre la ram : 
```
stress --vm 1 --vm-bytes 1024M
```
gg à netdata : 
```
netdata on web.tp2.linux
BOT
 — Aujourd’hui à 15:22
web.tp2.linux is critical, system.ram (ram), ram usage = 57.6%
ram usage = 57.6%
The percentage of RAM used by the system.
system.ram
ram


web.tp2.linux•Aujourd’hui à 15:22
```

![stress test](./pics/stress-test.jpg)

# II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |

🖥️ **VM `backup.tp2.linux`**

**Déroulez la [📝**checklist**📝](#checklist) sur cette VM.**

## 1. Intwo bwo

**La *backup* consiste à extraire des données de leur emplacement original afin de les stocker dans un endroit dédié.**  

**Cet endroit dédié est un endroit sûr** : le but est d'assurer la perennité des données sauvegardées, tout en maintenant leur niveau de sécurité.

Pour la sauvegarde, il existe plusieurs façon de procéder. Pour notre part, nous allons procéder comme suit :

- **création d'un serveur de stockage**
  - il hébergera les sauvegardes de tout le monde
  - ce sera notre "endroit sûr"
  - ce sera un partage NFS
  - ainsi, toutes les machines qui en ont besoin pourront accéder à un dossier qui leur est dédié sur ce serveur de stockage, afin d'y stocker leurs sauvegardes
- **développement d'un script de backup**
  - ce script s'exécutera en local sur les machines à sauvegarder
  - il s'exécute à intervalles de temps réguliers
  - il envoie les données à sauvegarder sur le serveur NFS
  - du point de vue du script, c'est un dossier local. Mais en réalité, ce dossier est monté en NFS.

![You're supposed to backup everything](./pics/backup_meme.jpg)

## 2. Partage NFS

🌞 **Setup environnement**

```
[dodo@backup backup]$ ls -al
total 0
drwxr-xr-x. 3 root root 27 Oct 12 17:12 .
drwxr-xr-x. 4 root root 34 Oct 12 17:10 ..
drwxr-xr-x. 2 root root  6 Oct 12 17:12 web.tp2.linux
```

🌞 **Setup partage NFS**

commande history : 
```
  360  sudo dnf -y install nfs-utils
  361  sudo vi /etc/idmapd.conf
  362  clear
  363  sudo vi /etc/exports
  364  sudo mkdir /home/nfsshare
  365  sudo systemctl enable --now rpcbind nfs-server
  366  sudo firewall-cmd --add-service=nfs
  367  sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind}
  368  sudo firewall-cmd --runtime-to-permanent
```

🌞 **Setup points de montage sur `web.tp2.linux`**

Configuration du client : 
```
[dodo@web ~]$ sudo dnf -y install nfs-utils
[dodo@web ~]$ sudo vi /etc/idmapd.conf
[dodo@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backups /srv/backup
```
Pour vérifier si tout va bien : 
```
[dodo@web ~]$ df -h
Filesystem                                   Size  Used Avail Use% Mounted on
devtmpfs                                     891M     0  891M   0% /dev
tmpfs                                        909M  340K  909M   1% /dev/shm
tmpfs                                        909M  8.6M  901M   1% /run
tmpfs                                        909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root                          6.2G  3.7G  2.6G  59% /
/dev/sda1                                   1014M  241M  774M  24% /boot
/dev/mapper/D1-D1_3                          976M  2.6M  907M   1% /mnt/part3
/dev/mapper/D1-D1_2                          976M  2.6M  907M   1% /mnt/part2
/dev/mapper/D1-D1_1                          976M  2.6M  907M   1% /mnt/part1
backup.tp2.linux:/srv/backups/web.tp2.linux  6.2G  3.6G  2.6G  59% /srv/backup
tmpfs                                        182M     0  182M   0% /run/user/1000
```
```
[dodo@web ~]$ mount
[...]
backup.tp2.linux:/srv/backups/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=262144,wsize=262144,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
[...]
```
```
[dodo@web WebDir]$ touch test
```
et hop : 
```
[dodo@web WebDir]$ ls
test
```
Et pour le montage automatique, on rajoute ça dans /etc/fstab : 
```
backup.tp2.linux:/srv/backups/web.tp2.linux /srv/backup/               nfs     defaults        0 0
```

## 3. Backup de fichiers

**Un peu de scripting `bash` !** Le scripting est le meilleur ami de l'admin, vous allez pas y couper hihi.  

La syntaxe de `bash` est TRES particulière, mais ce que je vous demande de réaliser là est un script minimaliste.

Votre script **DEVRA**...

- comporter un shebang
- comporter un commentaire en en-tête qui indique le but du script, en quelques mots
- comporter un commentaire qui indique l'auteur et la date d'écriture du script

Par exemple :

```bash
#!/bin/bash
# Simple backup script
# it4 - 09/10/2021

...
```

🌞 **Rédiger le script de backup `/srv/tp2_backup.sh`**

- le script crée une archive compressée `.tar.gz` du dossier ciblé
  - cela se fait avec la commande `tar`
- l'archive générée doit s'appeler `tp2_backup_YYMMDD_HHMMSS.tar.gz`
  - vous remplacerez évidemment `YY` par l'année (`21`), `MM` par le mois (`10`), etc.
  - ces infos sont déterminées dynamiquement au moment où le script s'exécute à l'aide de la commande `date`
- le script utilise la commande `rsync` afin d'envoyer la sauvegarde dans le dossier de destination
- il **DOIT** pouvoir être appelé de la sorte :

```bash
$ ./tp2_backup.sh <DESTINATION> <DOSSIER_A_BACKUP>
```

[backup_script.sh](tp2LinuxPart2/backup_script.sh)


🌞 **Tester le bon fonctionnement**

- exécuter le script sur le dossier de votre choix
- prouvez que la backup s'est bien exécutée
- **tester de restaurer les données**
  - récupérer l'archive générée, et vérifier son contenu

Execution du script : 
```
[dodo@backup srv]$ sudo ./backup_script.sh /home/dodo/backup /srv/backups/web.tp2.linux
```
Vérification : 
```
[dodo@backup backup]$ ls
Backup_211024_145815.tar.gz
```
Restauration : 
```
[dodo@backup backup]$ tar -xvf Backup_211024_145815.tar.gz
srv/backups/web.tp2.linux/
srv/backups/web.tp2.linux/test
[dodo@backup backup]$ ls
Backup_211024_145815.tar.gz  srv
[dodo@backup backup]$ cd srv
[dodo@backup srv]$ cd backups/
[dodo@backup backups]$ cd web.tp2.linux/
[dodo@backup web.tp2.linux]$ cat test
test1
```
Le script a bien fonctionné !


## 4. Unité de service

Lancer le script à la main c'est bien. **Le mettre dans une joulie *unité de service* et l'exécuter à intervalles réguliers, de manière automatisée, c'est mieux.**

Le but va être de créer un *service* systemd pour que vous puissiez interagir avec votre script de sauvegarde en faisant :

```bash
$ sudo systemctl start tp2_backup
$ sudo systemctl status tp2_backup
```

Ensuite on créera un *timer systemd* qui permettra de déclencher le lancement de ce *service* à intervalles réguliers.

**La classe nan ?**

---

### A. Unité de service

🌞 **Créer une *unité de service*** pour notre backup

- c'est juste un fichier texte hein
- doit se trouver dans le dossier `/etc/systemd/system/`
- doit s'appeler `tp2_backup.service`
- le contenu :

Et hop all in one : 
```
[dodo@backup system]$ cat tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/backup_script.sh /home/dodo/backup /srv/backups/web.tp2.linux
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

> Pour les tests, sauvegardez le dossier de votre choix, peu importe lequel.

🌞 **Tester le bon fonctionnement**

- n'oubliez pas d'exécuter `sudo systemctl daemon-reload` à chaque ajout/modification d'un *service*
- essayez d'effectuer une sauvegarde avec `sudo systemctl start backup`
- prouvez que la backup s'est bien exécutée
  - vérifiez la présence de la nouvelle archive
  
On reload :
```
[dodo@backup ~]$ sudo systemctl daemon-reload
```
Plus qu'à test : 
```
[dodo@backup ~]$ sudo systemctl start tp2_backup
```

C'est good !
```
[dodo@backup backup]$ ls -al
total 8
drwxrwxr-x. 3 dodo dodo  87 Oct 24 15:16 .
drwx------. 6 dodo dodo 245 Oct 24 14:58 ..
-rw-r--r--. 1 root root 174 Oct 24 14:58 Backup_211024_145815.tar.gz
-rw-r--r--. 1 root root 174 Oct 24 15:16 Backup_211024_151630.tar.gz
drwxrwxr-x. 3 dodo dodo  21 Oct 24 15:02 srv
```
---

### B. Timer

Un *timer systemd* permet l'exécution d'un *service* à intervalles réguliers.

🌞 **Créer le *timer* associé à notre `tp2_backup.service`**

- toujours juste un fichier texte
- dans le dossier `/etc/systemd/system/` aussi
- fichier `tp2_backup.timer`
- contenu du fichier :

La configuration : 
```
[dodo@backup ~]$ cd /etc/systemd/system/
[dodo@backup system]$ sudo cat tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

🌞 **Activez le timer**

- démarrer le *timer* : `sudo systemctl start tp2_backup.timer`
- activer le au démarrage avec une autre commande `systemctl`
- prouver que...
  - le *timer* est actif actuellement
  - qu'il est paramétré pour être actif dès que le système boot

Démarage du timer : 
```
[dodo@backup system]$ sudo systemctl start tp2_backup.timer
[dodo@backup system]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
[dodo@backup system]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Sun 2021-10-24 15:45:28 CEST; 47s ago
  Trigger: Sun 2021-10-24 15:47:00 CEST; 43s left

Oct 24 15:45:28 backup.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.
```

🌞 **Tests !**

- avec la ligne `OnCalendar=*-*-* *:*:00`, le *timer* déclenche l'exécution du *service* toutes les minutes
- vérifiez que la backup s'exécute correctement

On peut voir que les trois dernières backups ont 1 minutes d'écart entre leur création : 
```
[dodo@backup backup]$ ls -al
total 20
drwxrwxr-x. 3 dodo dodo 192 Oct 24 15:47 .
drwx------. 6 dodo dodo 221 Oct 24 15:35 ..
-rw-r--r--. 1 root root 174 Oct 24 14:58 Backup_211024_145815.tar.gz
-rw-r--r--. 1 root root 174 Oct 24 15:16 Backup_211024_151630.tar.gz
-rw-r--r--. 1 root root 174 Oct 24 15:45 Backup_211024_154528.tar.gz
-rw-r--r--. 1 root root 174 Oct 24 15:46 Backup_211024_154601.tar.gz
-rw-r--r--. 1 root root 174 Oct 24 15:47 Backup_211024_154702.tar.gz
drwxrwxr-x. 3 dodo dodo  21 Oct 24 15:02 srv
```
---

### C. Contexte

🌞 **Faites en sorte que...**

- votre backup s'exécute sur la machine `web.tp2.linux` :
```
[dodo@web system]$ sudo systemctl start tp2_backup.service
[dodo@web system]$ sudo systemctl status tp2_backup.service
● tp2_backup.service - Our own lil backup service (TP2)
   Loaded: loaded (/etc/systemd/system/tp2_backup.service; disabled; vendor preset: disabled)
   Active: inactive (dead)

Oct 24 16:25:49 web.tp2.linux systemd[1]: Starting Our own lil backup service (TP2)...
Oct 24 16:26:19 web.tp2.linux backup_script.sh[4084]: [OK] Archive //Backup_211024_162549.tar.gz created.
Oct 24 16:26:21 web.tp2.linux backup_script.sh[4084]: [OK] Archive //Backup_211024_162549.tar.gz synchronized to /srv/backup.
Oct 24 16:26:21 web.tp2.linux backup_script.sh[4084]: [OK] Directory /srv/backup cleaned to keep only the 5 most recent backups.
Oct 24 16:26:21 web.tp2.linux systemd[1]: tp2_backup.service: Succeeded.
Oct 24 16:26:21 web.tp2.linux systemd[1]: Started Our own lil backup service (TP2).
```
- le dossier sauvegardé est celui qui contient le site NextCloud et la destination est le dossier NFS monté depuis le serveur `backup.tp2.linux` :
```
[dodo@web system]$ sudo cat tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/backup_script.sh /srv/backup /var/www
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```
- la sauvegarde s'exécute tous les jours à 03h15 du matin :
```
[dodo@web system]$ sudo cat tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=--* 3:15:00

[Install]
WantedBy=timers.target
```
- prouvez avec la commande `sudo systemctl list-timers` que votre *service* va bien s'exécuter la prochaine fois qu'il sera 03h15 : 
```
[dodo@web system]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED       UNIT                         ACTIVATES
Sun 2021-10-24 17:50:20 CEST  1h 15min left Sun 2021-10-24 16:04:11 CEST  30min ago    dnf-makecache.timer          dnf-makecache.service
Mon 2021-10-25 03:15:00 CEST  10h left      n/a                           n/a          tp2_backup.timer             tp2_backup.service
Mon 2021-10-25 13:47:03 CEST  21h left      Sun 2021-10-24 13:47:03 CEST  2h 47min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

3 timers listed.
Pass --all to see loaded but inactive timers, too.
```

[tp2_backup.timer](tp2LinuxPart2/tp2_backup.timer)
[tp2_backup.service](tp2LinuxPart2/tp2_backup.service)


## 6. Petit point sur la backup

A ce stade vous avez :

- un script qui tourne sur `web.tp2.linux` et qui **sauvegarde les fichiers de NextCloud**
- un script qui tourne sur `db.tp2.linux` et qui **sauvegarde la base de données de NextCloud**
- toutes **les backups sont centralisées** sur `backup.tp2.linux`
- **tout est géré de façon automatisée**
  - les scripts sont packagés dans des *services*
  - les services sont déclenchés par des *timers*
  - tout est paramétré pour s'allumer quand les machines boot (les *timers* comme le serveur NFS)

🔥🔥 **That is clean shit.** 🔥🔥

# III. Reverse Proxy

## 1. Introooooo

Un *reverse proxy* est un outil qui sert d'intermédiaire entre le client et un serveur donné (souvent un serveur Web).

**C'est l'admin qui le met en place, afin de protéger l'accès au serveur Web.**

Une fois en place, le client devra saisir l'IP (ou le nom) du *reverse proxy* pour accéder à l'application Web (ce ne sera plus directement l'IP du serveur Web).

Un *reverse proxy* peut permettre plusieurs choses :

- chiffrement
  - c'est lui qui mettra le HTTPS en place (protocole HTTP + chiffrement avec le protocole TLS)
  - on pourrait le faire directement avec le serveur Web (Apache) dans notre cas
  - pour de meilleures performances, il est préférable de dédier une machine au chiffrement HTTPS, et de laisser au serveur web un unique job : traiter les requêtes HTTP
- répartition de charge
  - plutôt qu'avoir un seul serveur Web, on peut en setup plusieurs
  - ils hébergent tous la même application
  - le *reverse proxy* enverra les clients sur l'un ou l'autre des serveurs Web, afin de répartir la charge à traiter
- d'autres trucs
  - caching de ressources statiques (CSS, JSS, images, etc.)
  - tolérance de pannes
  - ...

---

**Dans ce TP on va setup un reverse proxy NGINX très simpliste.**

![Apache at the back hihi](./pics/nginx-at-the-front-apache-at-the-back.jpg)

## 2. Setup simple

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | ?           | ?             |

🖥️ **VM `front.tp2.linux`**

**Déroulez la [📝**checklist**📝](#checklist) sur cette VM.**

🌞 **Installer NGINX**

- vous devrez d'abord installer le paquet `epel-release` avant d'installer `nginx`
  - EPEL c'est des dépôts additionnels pour Rocky
  - NGINX n'est pas présent dans les dépôts par défaut que connaît Rocky
- le fichier de conf principal de NGINX est `/etc/nginx/nginx.conf`

Installation du package : 
```
[dodo@front ~]$ sudo dnf install -y epel-release
```
Installation d'nginx : 
```
[dodo@front ~]$ sudo dnf install -y nginx
```

🌞 **Tester !**

- lancer le *service* `nginx`
- le paramétrer pour qu'il démarre seul quand le système boot
- repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall
- vérifier que vous pouvez joindre NGINX avec une commande `curl` depuis votre PC

Le démarage : 
```
[dodo@front ~]$ sudo systemctl start nginx
```
Enable pour démarer au boot : 
```
[dodo@front ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
```
Le port : 
```
[dodo@front ~]$ sudo cat /etc/nginx/nginx.conf
[...]
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
[...]
```
Ca fonctionne ! : 
```
[dodo@front ~]$ sudo curl 10.102.1.14:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
[...]
```


🌞 **Explorer la conf par défaut de NGINX**

- repérez l'utilisateur qu'utilise NGINX par défaut
- dans la conf NGINX, on utilise le mot-clé `server` pour ajouter un nouveau site
  - repérez le bloc `server {}` dans le fichier de conf principal
- par défaut, le fichier de conf principal inclut d'autres fichiers de conf
  - mettez en évidence ces lignes d'inclusion dans le fichier de conf principal


Nginx utilise root comme user par défaut : 
```
[dodo@front ~]$ sudo cat /etc/nginx/nginx.conf
[...]
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
[...]
```
Hop le server nginx : 
```
[dodo@front ~]$ sudo cat /etc/nginx/nginx.conf
[...]
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
[...]
```

Pour inclure d'autres fichiers de conf on utilise ```include```, exemple : 
```
include /usr/share/nginx/modules/*.conf;
```

🌞 **Modifier la conf de NGINX**

- pour que ça fonctionne, le fichier `/etc/hosts` de la machine **DOIT** être rempli correctement, conformément à la **[📝**checklist**📝](#checklist)**
- supprimer le bloc `server {}` par défaut, pour ne plus présenter la page d'accueil NGINX
- créer un fichier `/etc/nginx/conf.d/web.tp2.linux.conf` avec le contenu suivant :
  - j'ai sur-commenté pour vous expliquer les lignes, n'hésitez pas à dégommer mes lignes de commentaires

Résolution du nom de web : 
```
[dodo@front ~]$ sudo cat /etc/hosts
[...]
10.102.1.11 web.tp2.linux
[...]
```
On a bien supprimé le server de base nginx.
Et on crée un nouveau serveur : 
```
[dodo@front conf.d]$ sudo cat web.tp2.linux.conf

server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```

On arrive bien sur la page de nextcloud et plus sur la page d'accueil de nginx : 
```
[dodo@front conf.d]$ sudo curl 10.102.1.14:80
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```